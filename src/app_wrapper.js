import React, { useState, useEffect } from "react"
import { Switch, Route, Redirect, useLocation } from "react-router-dom"
import { CSSTransition, TransitionGroup } from "react-transition-group"
import { useDispatch } from "react-redux"

import App from "./App"
import Authentication from "./components/auth"
import ConfigDB from "./data/customizer/config"
import { routes } from "./route"

import UserService from "./network/services/user"
import CurrencyService from "./network/services/currency"
import Loader from "./layout/loader"

const AppWrapper = () => {
  const dispatch = useDispatch()
  const location = useLocation()
  const [anim, setAnim] = useState("")
  const [notLoggedIn, setNotLoggedIn] = useState(null)

  const animation = localStorage.getItem("animation") || ConfigDB.data.router_animation || "fade"

  const init = async () => {
    setAnim(animation)
    console.ignoredYellowBox = ["Warning: Each", "Warning: Failed"]
    console.disableYellowBox = true

    const storedJwt = localStorage.getItem("token")
    const storedUser = localStorage.getItem("user")

    if (storedJwt != null && storedUser != null) {
      const updatedUser = await UserService.getMyself()
      console.log(updatedUser)
      dispatch({ type: "SET_USER", user: updatedUser.user })
      dispatch({ type: "SET_AGENT", partners: updatedUser.partners })
      dispatch({ type: "SET_PROFILE", profile: updatedUser.profile ?? {} })

      if (updatedUser.verification) {
        dispatch({ type: "SET_VERIFICATION", profile: updatedUser.verification })
      }

      if (updatedUser.auth_required) {
        dispatch({ type: "SET_TWOFA", twoFA: updatedUser.auth_required }) // set true
      }

      const currencyResponse = await CurrencyService.get()
      dispatch({
        type: "SET_CURRENCIES",
        currencies: currencyResponse.currencies,
      })
      setNotLoggedIn(false)
      console.log(location)
      dispatch({ type: "SET_ACTIVE_MENU", path: location.pathname })
    } else {
      setNotLoggedIn(true)
    }
  }

  useEffect(() => {
    init()
    // eslint-disable-next-line
  }, [])

  return (
    <>
      {notLoggedIn != null ? (
        <Switch>
          {!notLoggedIn ? (
            <App>
              <Route
                exact
                path={`${process.env.PUBLIC_URL}/`}
                render={() => {
                  return <Redirect to={`${process.env.PUBLIC_URL}/dashboard`} />
                }}
              />
              <Route
                exact
                path={`${process.env.PUBLIC_URL}/auth`}
                render={() => {
                  return <Redirect to={`${process.env.PUBLIC_URL}/dashboard`} />
                }}
              />
              <TransitionGroup>
                {routes.map(({ path, Component }) => (
                  <Route key={path} exact path={`${process.env.PUBLIC_URL}${path}`}>
                    {({ match }) => (
                      <CSSTransition
                        in={match != null}
                        timeout={100}
                        classNames={anim}
                        unmountOnExit
                        exit={false}
                      >
                        <div>
                          <Component />
                        </div>
                      </CSSTransition>
                    )}
                  </Route>
                ))}
              </TransitionGroup>
            </App>
          ) : (
            <>
              <Route path={`${process.env.PUBLIC_URL}/auth`} component={Authentication} />
              <Redirect to={`${process.env.PUBLIC_URL}/auth`} />
            </>
          )}
        </Switch>
      ) : (
        <Loader />
      )}
    </>
  )
}

export default AppWrapper
