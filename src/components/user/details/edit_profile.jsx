import React, { useEffect, useState, useMemo } from "react"
import { Form, FormGroup, Label, Input, Card, CardBody, Button, Table } from "reactstrap"
import { useForm } from "react-hook-form"
import { useSelector } from "react-redux"
import { toast } from "react-toastify"

import UserService from "../../../network/services/user"

const EditProfile = () => {
  const user = useSelector((state) => state.user.user)
  const profile = useSelector((state) => state.user.profile)
  // const partners = useSelector((state) => state.user.partners)
  const bank = useSelector((state) => state.user.bank)

  const [isEditable, setIsEditable] = useState(false)
  const { register, handleSubmit, errors, setValue, reset } = useForm()
  const submitUpdate = async (data) => {
    console.log(data)

    try {
      if (bank == null) {
        const result = await UserService.createBank(data)
        console.log(result)

        if (result.bank) {
          reset()
          setIsEditable(false)
          toast.success("Bank updated successfully", {
            position: toast.POSITION.TOP_RIGHT,
          })
        }
      } else {
        const result = await UserService.updateBank(bank.id, data)
        console.log(result)

        if (result.bank) {
          reset()
          setIsEditable(false)
          toast.success("Bank updated successfully", {
            position: toast.POSITION.TOP_RIGHT,
          })
        }
      }
    } catch (error) {
      console.log(error)
      toast.error(error.message, {
        position: toast.POSITION.TOP_RIGHT,
      })
    }
  }

  const address = useMemo(() => {
    let addr = ""

    if (profile != null) {
      addr += profile.street + ", "
      addr += profile.building + ", "
      addr += profile.unit + ", "
      addr += profile.city + ", "
      addr += profile.zip + ", "
      addr += profile.state + ", "
      addr += profile.country
    }

    return addr
  }, [profile])

  // const ib = useMemo(() => {
  //   if (partners != null) {
  //     return partners?.map((partner) => partner.ib_code)?.join(",")
  //   }
  // }, [partners])

  useEffect(() => {
    if (isEditable) {
      setValue("account_holder", bank?.account_holder)
      setValue("bank_branch", bank?.bank_branch)
      setValue("account_number", bank?.account_number)
      setValue("swift_code", bank?.swift_code)
      setValue("bank_name", bank?.bank_name)
    }
  // eslint-disable-next-line
  }, [isEditable])

  return (
    <Card>
      <CardBody>
        <Table borderless>
          <tbody>
            <tr>
              <td width="50%">Name</td>
              <td>{user?.name ?? "-"}</td>
            </tr>
            <tr>
              <td>Email</td>
              <td>{user?.email ?? "-"}</td>
            </tr>
            <tr>
              <td>Address</td>
              <td>{address ?? "-"}</td>
            </tr>
            <tr>
              <td>Phone number</td>
              <td>{user?.mobile ?? "-"}</td>
            </tr>
            {/* <tr>
              <td>IB Account</td>
              <td>{ib ?? "-"}</td>
            </tr> */}
            <tr>
              <td style={{ verticalAlign: "middle" }}>Bank Details</td>
              <td>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <span>{bank?.bank_name ?? "-"}</span>
                  <Button
                    onClick={() => {
                      setIsEditable(!isEditable)
                    }}
                  >
                    {isEditable ? "Cancel" : "Edit"}
                  </Button>
                </div>
              </td>
            </tr>
          </tbody>
        </Table>
        {isEditable && (
          <Form
            className="theme-form"
            onSubmit={handleSubmit(submitUpdate)}
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              padding: "0.75rem",
            }}
          >
            <h4>Edit Bank Details</h4>
            <FormGroup>
              <Label className="col-form-label pt-0">Account Holder</Label>
              <Input
                className="form-control"
                type="text"
                name="account_holder"
                innerRef={register({ required: true })}
              />
              <span style={{ color: "red" }}>{errors.account_holder && "Name is required"}</span>
            </FormGroup>
            <FormGroup>
              <Label className="col-form-label pt-0">Bank Branch</Label>
              <Input
                className="form-control"
                type="text"
                name="bank_branch"
                innerRef={register({ required: true })}
              />
              <span style={{ color: "red" }}>{errors.bank_branch && "Branch is required"}</span>
            </FormGroup>
            <FormGroup>
              <Label className="col-form-label pt-0">IBAN/Account Number</Label>
              <Input
                className="form-control"
                type="text"
                name="account_number"
                innerRef={register({ required: true })}
              />
              <span style={{ color: "red" }}>
                {errors.account_number && "Account Number is required"}
              </span>
            </FormGroup>
            <FormGroup>
              <Label className="col-form-label pt-0">IFSC/SWIFT code</Label>
              <Input
                className="form-control"
                type="text"
                name="swift_code"
                innerRef={register({ required: true })}
              />
              <span style={{ color: "red" }}>{errors.swift_code && "Swift Code is required"}</span>
            </FormGroup>
            <FormGroup>
              <Label className="col-form-label pt-0">Bank Name</Label>
              <Input
                className="form-control"
                type="text"
                name="bank_name"
                innerRef={register({ required: true })}
              />
              <span style={{ color: "red" }}>{errors.bank_name && "Bank Name is required"}</span>
            </FormGroup>
            <Button color="primary btn-block" type="submit" style={{ maxWidth: 150 }}>
              Submit
            </Button>
          </Form>
        )}
      </CardBody>
    </Card>
  )
}

export default EditProfile
