import React from "react"
import DataTable from "react-data-table-component"
import moment from "moment"
import { useSelector } from "react-redux"

import IdentityUpload from "./identity_upload"

const columns = [
  {
    name: "Status",
    selector: "status",
    sortable: true,
    width: "100px",
    format: (row) => {
      switch (row.status) {
        case 0:
          return "Pending"
        case 1:
          return "Verified"
        case 2:
          return "Failed"
        default:
          return "-"
      }
    },
  },
  {
    name: "Upload Date",
    selector: "created_at",
    sortable: true,
    width: "200px",
    format: (row) => {
      return moment(row.created_at).format("yyyy-MM-DD hh:mmA")
    },
  },
  {
    name: "Front",
    selector: "id_front",
    sortable: true,
    format: (row) => {
      return (
        <a href={row.id_front} target="_blank" rel="noopener noreferrer">
          {row.id_front}
        </a>
      )
    },
  },
  {
    name: "Back",
    selector: "id_back",
    sortable: true,
    format: (row) => {
      return (
        <a href={row.id_back} target="_blank" rel="noopener noreferrer">
          {row.id_back}
        </a>
      )
    },
  },
]

const IdentityTable = () => {
  const verification = useSelector((state) => state.user.verification)

  if (verification == null) {
    return <IdentityUpload />
  }

  return <DataTable noHeader data={[verification]} columns={columns} striped={true} center={true} />
}

export default IdentityTable
