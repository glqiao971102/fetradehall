import React from "react"
import { useDispatch } from "react-redux"
import { Row, Col, ListGroup, ListGroupItem, Input, FormText } from "reactstrap"
import styled from "styled-components"

const MobileGap = styled.div`
  padding: 0;

  @media (max-width: 575px) {
    padding: 10px;
  }
`

const AddressUpload = () => {
  const dispatch = useDispatch()

  return (
    <>
      <Row>
        <Col md="6">
          <ListGroup className="list-group-flush">
            <ListGroupItem>
              Must be dated not older than three months at the time of verification
            </ListGroupItem>
            <ListGroupItem>
              Document must show residence address, name and match registration details
            </ListGroupItem>
            <ListGroupItem>
              All information and details must be clearly visible (financial information can be
              hidden)
            </ListGroupItem>
            <ListGroupItem>Must be presented in full</ListGroupItem>
            <ListGroupItem>
              ID card can be accepted for address proof if ID card is valid and include the address
            </ListGroupItem>
            <ListGroupItem>The edges of documents should not be cut off</ListGroupItem>
          </ListGroup>
        </Col>
        <Col md="6">
          <img src="/address.jpg" alt="address" style={{ maxWidth: "100%" }} />
        </Col>
      </Row>

      <hr />
      <p>Front and back of your address proof</p>

      <Row>
        <Col sm={12} md={6}>
          <FormText color="muted">Address Front</FormText>
          <Input
            type="file"
            name="file"
            accept="image/png, image/jpeg, application/pdf"
            onChange={(e) => {
              dispatch({ type: "SET_ADDRESS_FRONT", addressFront: e?.currentTarget?.files[0] })
            }}
          />
          <FormText color="muted">.jpg, .png, pdf, max 10 MB</FormText>
          <MobileGap />
        </Col>
        <Col sm={12} md={6}>
          <FormText color="muted">Address Back</FormText>
          <Input
            type="file"
            name="file"
            accept="image/png, image/jpeg, application/pdf"
            onChange={(e) => {
              dispatch({ type: "SET_ADDRESS_BACK", addressBack: e?.currentTarget?.files[0] })
            }}
          />
          <FormText color="muted">.jpg, .png, pdf, max 10 MB</FormText>
        </Col>
      </Row>
    </>
  )
}

export default AddressUpload
