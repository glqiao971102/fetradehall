import React from 'react'
import { Nav, NavItem, NavLink, TabContent, TabPane, Row, Col, FormText, Input } from 'reactstrap'
import styled from 'styled-components'

import Passport from './identity/passport'
import NationalId from './identity/national_id'
import DrivingLicense from './identity/driving_license'
import { useDispatch, useSelector } from 'react-redux'

const MobileGap = styled.div`
  padding: 0;

  @media (max-width: 575px) {
    padding: 10px;
  }
`

const IdentityUpload = () => {
  const dispatch = useDispatch()
  const activeTab = useSelector((state) => state.verification.activeTab)

  return (
    <>
      <Nav className="nav-pills nav-primary">
        <NavItem>
          <NavLink
            className={activeTab === 'passport' ? 'active' : ''}
            onClick={() => dispatch({ type: 'SET_ACTIVE_VERIFICATION', activeTab: 'passport' })}
            style={{ cursor: 'pointer' }}
          >
            Passport
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={activeTab === 'license' ? 'active' : ''}
            onClick={() => dispatch({ type: 'SET_ACTIVE_VERIFICATION', activeTab: 'license' })}
            style={{ cursor: 'pointer' }}
          >
            Driving License
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={activeTab === 'id' ? 'active' : ''}
            onClick={() => dispatch({ type: 'SET_ACTIVE_VERIFICATION', activeTab: 'id' })}
            style={{ cursor: 'pointer' }}
          >
            National ID
          </NavLink>
        </NavItem>
      </Nav>

      <div style={{ padding: 16 }} />

      <TabContent activeTab={activeTab}>
        <TabPane tabId="passport">
          <Passport />
        </TabPane>
        <TabPane tabId="driving_license">
          <DrivingLicense />
        </TabPane>
        <TabPane tabId="national_id">
          <NationalId />
        </TabPane>
      </TabContent>

      <hr />
      <p>Front and back of your {activeTab} proof</p>

      <Row>
        <Col sm={12} md={6}>
          <FormText color="muted" style={{ textTransform: 'capitalize' }}>
            {activeTab} Front
          </FormText>
          <Input
            type="file"
            name="file"
            accept="image/png, image/jpeg, application/pdf"
            onChange={(e) => {
              dispatch({ type: 'SET_ID_FRONT', idFront: e?.currentTarget?.files[0] })
            }}
          />
          <FormText color="muted">.jpg, .png, pdf, max 10 MB</FormText>
          <MobileGap />
        </Col>
        <Col sm={12} md={6}>
          <FormText color="muted" style={{ textTransform: 'capitalize' }}>
            {activeTab} Back
          </FormText>
          <Input
            type="file"
            name="file"
            accept="image/png, image/jpeg, application/pdf"
            onChange={(e) => {
              dispatch({ type: 'SET_ID_BACK', idBack: e?.currentTarget?.files[0] })
            }}
          />
          <FormText color="muted">.jpg, .png, pdf, max 10 MB</FormText>
        </Col>
      </Row>
    </>
  )
}

export default IdentityUpload
