import React from "react"
import DataTable from "react-data-table-component"
import moment from "moment"
import { useSelector } from "react-redux"

import AddressUpload from "./address_upload"

const columns = [
  {
    name: "Status",
    selector: "status",
    sortable: true,
    width: "100px",
    format: (row) => {
      switch (row.status) {
        case 0:
          return "Pending"
        case 1:
          return "Verified"
        case 2:
          return "Failed"
        default:
          return "-"
      }
    },
  },
  {
    name: "Upload Date",
    selector: "created_at",
    sortable: true,
    width: "200px",
    format: (row) => {
      return moment(row.created_at).format("yyyy-MM-DD hh:mmA")
    },
  },
  {
    name: "Front",
    selector: "address_front",
    sortable: true,
    format: (row) => {
      return (
        <a href={row.address_front} target="_blank" rel="noopener noreferrer">
          {row.address_front}
        </a>
      )
    },
  },
  {
    name: "Back",
    selector: "address_back",
    sortable: true,
    format: (row) => {
      return (
        <a href={row.address_back} target="_blank" rel="noopener noreferrer">
          {row.address_back}
        </a>
      )
    },
  },
]

const AddressTable = () => {
  const verification = useSelector((state) => state.user.verification)

  if (verification == null) {
    return (
      <>
        <p>
          Our compliance requires that we verify your address of residence. To do so, you need to
          upload document confirming your address. This document must be issued by government
          agency, local utility company or a financial institution. Please remember that all details
          in your profile must match those on the documents you submit.
        </p>
        <AddressUpload />
      </>
    )
  }

  return <DataTable noHeader data={[verification]} columns={columns} striped={true} center={true} />
}

export default AddressTable
