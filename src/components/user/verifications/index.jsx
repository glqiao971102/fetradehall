import React from 'react'
import { Card, CardHeader, CardBody, Button, Spinner } from 'reactstrap'
import { useSelector } from 'react-redux'
import _ from 'lodash'

import AddressTable from './address_table'
import IdentityTable from './identity_table'

import VerificationService from '../../../network/services/verification'
import FilesService from '../../../network/services/files'
import { BASE_URL } from '../../../network/constants'
import { useState } from 'react'

const Verification = () => {
  const [isBusy, setIsBusy] = useState(false)
  const profile = useSelector((state) => state.user.profile)
  const userVerification = useSelector((state) => state.user.verification)
  const verification = useSelector((state) => state.verification)

  const handleVerification = async (e) => {
    console.log(verification)
    setIsBusy(true)

    try {
      const idFrontRes = await FilesService.create(verification.idFront)
      const idBackRes = await FilesService.create(verification.idBack)
      const addressFrontRes = await FilesService.create(verification.addressFront)
      const addressBackRes = await FilesService.create(verification.addressBack)

      const result = await VerificationService.create({
        type: verification.activeTab,
        id_front: BASE_URL + '/' + idFrontRes.data,
        id_back: BASE_URL + '/' + idBackRes.data,
        address_front: BASE_URL + '/' + addressFrontRes.data,
        address_back: BASE_URL + '/' + addressBackRes.data
      })

      console.log(result)

      if (result?.verification) {
        window.location.reload()
        setIsBusy(false)
      }
    } catch (error) {
      setIsBusy(false)
      console.log(error)
    }
  }

  if (profile != null && !_.isEmpty(profile)) {
    return (
      <>
        {userVerification == null && (
          <Card>
            <CardBody>
              <p style={{ margin: 0 }}>
                Your profile isn’t verified. Please verify your profile to take full advantage of
                our services!
              </p>
            </CardBody>
          </Card>
        )}

        <Card>
          <CardHeader>
            <h5>Identification</h5>
            <div className="p-1" />
            <p style={{ margin: 0 }}>Passport / National ID / Driving License</p>
            <div className="p-0" />
            <p style={{ margin: 0 }}>
              Please upload an identification document, in colour, where your full name is
              displayed.
            </p>
          </CardHeader>
          <CardBody>
            <IdentityTable />
          </CardBody>
        </Card>
        <Card>
          <CardHeader>
            <h5>Proof Of Address</h5>
            <div className="p-1" />
            <p style={{ margin: 0 }}>
              Proof of address that is not older than 180 days (utility bill / bank statement /
              other official document with your name and address on it)
            </p>
          </CardHeader>
          <CardBody>
            <AddressTable />
          </CardBody>
        </Card>

        {userVerification == null && (
          <Card>
            <CardBody style={{ textAlign: 'center' }}>
              <h6>
                By clicking "Confirm & Proceed" you confirm that all the information provided is
                accurate and valid
              </h6>
              <Button color="primary" onClick={handleVerification} disabled={isBusy}>
                {isBusy ? 'Loading' : 'Submit'}
              </Button>
            </CardBody>
          </Card>
        )}
      </>
    )
  }

  // new
  if (profile != null && _.isEmpty(profile)) {
    return (
      <Card>
        <CardBody>
          Please complete the <a href="#details">My Details Verification Process</a> to continue the
          process.
        </CardBody>
      </Card>
    )
  }

  // loading
  return (
    <Card>
      <CardBody style={{ textAlign: 'center' }}>
        <Spinner />
      </CardBody>
    </Card>
  )
}

export default Verification
