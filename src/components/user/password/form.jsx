import React, { useState } from "react"
import { Row, Col, Form, FormGroup, Label, Input, Button } from "reactstrap"
import { useForm } from "react-hook-form"

import UserService from "../../../network/services/user"
import { toast } from "react-toastify"

const ChangePasswordForm = () => {
  const { register, handleSubmit, errors, reset, formState } = useForm()
  const [error, setError] = useState(null)
  const { isSubmitting } = formState

  const handleChangePassword = async (data) => {
    console.log(data)
    try {
      if (data !== "") {
        const result = await UserService.changePassword(data)
        console.log(result)

        if (result.user) {
          reset()
          toast.success("Password changed successfully!", {
            position: toast.POSITION.TOP_RIGHT,
          })
        }
      } else {
        errors.showMessages()
      }
    } catch (error) {
      console.log(error)
      setError(error.message)
    }
  }

  return (
    <Form className="theme-form" onSubmit={handleSubmit(handleChangePassword)}>
      <Row>
        <Col>
          <FormGroup>
            <Label>Old Password</Label>
            <Input
              className="form-control"
              type="password"
              name="password"
              innerRef={register({ required: true })}
            />
            <span style={{ color: "red" }}>{errors.password && "Old password is required"}</span>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label>New Password</Label>
            <Input
              className="form-control"
              type="password"
              name="new_password"
              innerRef={register({ required: true })}
            />
            <span style={{ color: "red" }}>
              {errors.new_password && "New password is required"}
            </span>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label>Confirm Password</Label>
            <Input
              className="form-control"
              type="password"
              name="new_password_confirmation"
              innerRef={register({ required: true })}
            />
            <span style={{ color: "red" }}>
              {errors.new_password_confirmation && "Confirm password is required"}
            </span>
            {error != null && <span style={{ color: "red" }}>{error}</span>}
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup className="mb-0">
            <Button color="success" className="mr-3" disabled={isSubmitting}>
              {isSubmitting ? "Loading..." : "Confirm"}
            </Button>
          </FormGroup>
        </Col>
      </Row>
    </Form>
  )
}

export default ChangePasswordForm
