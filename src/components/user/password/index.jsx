import React from "react";
import { Card, CardBody } from "reactstrap";
import ChangePasswordForm from "./form";

const ChangePassword = () => {
  return (
    <Card>
      <CardBody>
        <ChangePasswordForm />
      </CardBody>
    </Card>
  );
};

export default ChangePassword;
