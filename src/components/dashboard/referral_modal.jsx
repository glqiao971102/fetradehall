import React from "react"
import { Card, CardHeader, CardBody, Col, Row, Button, Spinner } from "reactstrap"
import { useSelector } from "react-redux"
import { CopyToClipboard } from "react-copy-to-clipboard"
import { toast } from "react-toastify"

const ReferralModal = () => {
  const partners = useSelector((state) => state.user.partners)

  return (
    <div>
      {partners != null ? (
        partners?.length > 0 ? (
          partners.map((partner) => {
            return (
              <Card key={partner.id}>
                <CardHeader className="bg-secondary" style={{ padding: 20 }}>
                  <h5 className="text-center">IB-{partner?.ib_code}</h5>
                </CardHeader>
                <CardBody style={{ padding: 16, textAlign: "center" }}>
                  <p>https://tradehall.vercel.app/auth?referral_code={partner?.referral_code}</p>
                  <Row style={{ marginTop: 12 }}>
                    <Col className="text-right">
                      <CopyToClipboard
                        text={
                          "https://tradehall.vercel.app/auth?referral_code=" +
                          partner?.referral_code
                        }
                      >
                        <Button
                          color="primary"
                          onClick={() => {
                            toast.success("Link copied to clipboard!", {
                              position: toast.POSITION.BOTTOM_RIGHT,
                            })
                          }}
                        >
                          Copy Link
                        </Button>
                      </CopyToClipboard>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            )
          })
        ) : (
          <p>No record found</p>
        )
      ) : (
        <div style={{ textAlign: "center" }}>
          <Spinner />
        </div>
      )}

      <div style={{ textAlign: "center" }}>
        <h5>How it works?</h5>
        <Row>
          <Col>
            <img src="/refer_1.svg" alt="refer_1" style={{ height: 50, marginBottom: 12 }} />
            <p>Share your unique link with friends</p>
          </Col>
          <Col>
            <img src="/refer_2.svg" alt="refer_2" style={{ height: 50, marginBottom: 12 }} />
            <p>Your friends become traders</p>
          </Col>
          <Col>
            <img src="/refer_3.svg" alt="refer_3" style={{ height: 50, marginBottom: 12 }} />
            <p>You get rewarded for their every trade</p>
          </Col>
        </Row>
      </div>
    </div>
  )
}

export default ReferralModal
