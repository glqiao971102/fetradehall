import React from "react"
import DataTable from "react-data-table-component"
import moment from "moment"

const columns = [
  {
    name: "Ticket",
    selector: "id",
    sortable: true,
  },
  {
    name: "Date",
    selector: "updated_at",
    sortable: true,
    width: "200px",
    format: (row) => {
      return moment(row.updatedAt).format("yyyy-MM-DD HH:mmA")
    },
  },
  {
    name: "Wallet",
    selector: "currency_unit",
    sortable: true,
  },
  {
    name: "Action",
    selector: "txn_type",
    sortable: true,
    format: (row) => {
      switch (row.txn_type) {
        case 0:
          return "Deposit"
        case 1:
          return "Withdraw"
        case 2:
          return "Transfer"
        default:
          return "-"
      }
    },
  },
  {
    name: "Method",
    selector: "method",
    sortable: true,
    width: "150px",
    format: (row) => {
      switch (row.txn_type) {
        case 0:
          return row?.deposit?.payment_method
        case 1:
          return row?.withdraw?.payment_method ?? "-"
        case 2:
          return "Transfer"
        default:
          return "-"
      }
    },
  },
  {
    name: "Amount",
    selector: "debit_amount",
    sortable: true,
    format: (row) => {
      return row.debit_amount > 0 ? row.debit_amount : row.credit_amount
    },
  },
  {
    name: "Status",
    selector: "status",
    sortable: true,
    format: (row) => {
      switch (row.status) {
        case 0:
          return <span>Pending</span>
        case 1:
          return <span style={{ color: "green" }}>Success</span>
        case 2:
          return <span style={{ color: "red" }}>Failed</span>
        default:
          return "-"
      }
    },
  },
]

const WalletTable = ({ transactions }) => {
  return (
    <>
      <h5>Recent Transactions</h5>
      <DataTable
        noHeader
        data={transactions}
        columns={columns}
        striped={true}
        center={true}
      />
    </>
  )
}

export default WalletTable
