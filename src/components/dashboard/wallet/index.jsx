import React, { useEffect, useState } from 'react'
import { Row, Col, Card, CardHeader, CardBody, Button, Spinner } from 'reactstrap'
import { useHistory } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'

import WalletTable from './table'
import { ResponsiveDiv, StyledRow } from '../../common/components.style'

import TransactionService from '../../../network/services/transaction'

const Wallet = () => {
  let history = useHistory()
  const dispatch = useDispatch()
  const wallets = useSelector((state) => state.wallet.wallets)
  const totalDeposit = useSelector((state) => state.wallet.totalDeposit)
  const totalWithdrawal = useSelector((state) => state.wallet.totalWithdrawal)
  const totalAccountDeposit = useSelector((state) => state.wallet.totalAccountDeposit)
  const totalAccountWithdraw = useSelector((state) => state.wallet.totalAccountWithdraw)
  const [transactions, setTransactions] = useState(null)

  const init = async () => {
    const transactionResponse = await TransactionService.getAll()
    setTransactions(transactionResponse.transactions.data)
  }

  const navigate = (path) => {
    history.push(path)
    dispatch({ type: 'SET_ACTIVE_MENU', path: path })
  }

  useEffect(() => {
    init()
  }, [])

  return (
    <>
      {wallets.map((wallet) => {
        return (
          <Card className="card-absolute" key={wallet.id}>
            <CardHeader className="bg-primary">
              <h6 style={{ margin: 0 }}>
                {`#${wallet.id.toString().padStart(8, '0')} - ${wallet.currency?.abbreviation}`}
              </h6>
            </CardHeader>
            <div className="card-right">
              <ResponsiveDiv>
                <Button
                  onClick={() => {
                    navigate(`${process.env.PUBLIC_URL}/financial#deposit`)
                  }}
                >
                  Deposit
                </Button>
                <Button
                  onClick={() => {
                    navigate(`${process.env.PUBLIC_URL}/financial#withdrawal`)
                  }}
                >
                  Withdraw
                </Button>
                <Button
                  onClick={() => {
                    navigate(`${process.env.PUBLIC_URL}/financial#history`)
                  }}
                >
                  History
                </Button>
              </ResponsiveDiv>
            </div>
            <CardBody className="text-center">
              <div style={{ padding: 8 }} />
              <StyledRow>
                <Col sm={6} lg={4}>
                  <h6>{wallet.currency?.name}</h6>
                  <p>WALLET</p>
                </Col>
                <Col sm={6} lg={4}>
                  <h6>{`${wallet.balance} ${wallet.currency?.name}`}</h6>
                  <p>BALANCE</p>
                </Col>
                <Col sm={6} lg={4}>
                  <h6>{`${totalDeposit} ${wallet.currency?.name}`}</h6>
                  <p>TOTAL DEPOSITS</p>
                </Col>
                <Col sm={6} lg={4}>
                  <h6>{`${totalWithdrawal} ${wallet.currency?.name}`}</h6>
                  <p>TOTAL WITHDRAWALS</p>
                </Col>
                <Col sm={6} lg={4}>
                  <h6>{`${totalAccountDeposit} ${wallet.currency?.name}`}</h6>
                  <p>TOTAL MT5 ACCOUNT DEPOSIT</p>
                </Col>
                <Col sm={6} lg={4}>
                  <h6>{`${totalAccountWithdraw} ${wallet.currency?.name}`}</h6>
                  <p>TOTAL MT5 ACCOUNT WITHDRAWAL</p>
                </Col>
              </StyledRow>
            </CardBody>
          </Card>
        )
      })}

      <Card>
        <CardBody>
          {transactions != null ? (
            transactions?.length > 0 ? (
              <WalletTable transactions={transactions} />
            ) : (
              <h5>No recent transactions</h5>
            )
          ) : (
            <div style={{ textAlign: 'center' }}>
              <Spinner />
            </div>
          )}
        </CardBody>
      </Card>
    </>
  )
}

export default Wallet
