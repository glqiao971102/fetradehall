import React, { useState, useEffect } from "react"
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  Spinner,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
} from "reactstrap"
import { useHistory } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import styled from "styled-components"

import Breadcrumb from "../../layout/breadcrumb"
import Banner from "./banner"
// import DashboardCarousel from "./carousel";
import Wallet from "./wallet"
import Partner from "./partner"

import WalletService from "../../network/services/wallet"
import AccountService from "../../network/services/account"
import PromotionService from "../../network/services/promotion"
import ReferralModal from "./referral_modal"

const ResponsiveCard = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media (max-width: 575px) {
    flex-direction: column;
    align-items: flex-start;
  }
`

const ResponsiveButton = styled(Button)`
  margin-left: 12px;
  @media (max-width: 575px) {
    align-self: flex-end;
  }
`

const ResponsiveParagraph = styled.p`
  margin: 0;

  @media (max-width: 575px) {
    margin-bottom: 8px;
  }
`

const Dashboard = () => {
  const history = useHistory()
  const dispatch = useDispatch()
  const user = useSelector((state) => state.user.user)
  const [activeTab, setActiveTab] = useState("wallet")
  const [promotions, setPromotions] = useState(null)

  const init = async () => {
    const walletResponse = await WalletService.get()
    dispatch({ type: "SET_WALLETS", wallets: walletResponse.wallets })
    dispatch({
      type: "SET_BALANCE",
      balance: walletResponse.wallet_balance,
    })
    dispatch({
      type: "SET_TOTAL_DEPOSIT",
      totalDeposit: walletResponse.total_deposit,
    })
    dispatch({
      type: "SET_TOTAL_WITHDRAWAL",
      totalWithdrawal: walletResponse.total_withdrawal,
    })
    dispatch({
      type: "SET_TOTAL_ACCOUNT_DEPOSIT",
      totalAccountDeposit: walletResponse.total_account_deposit,
    })
    dispatch({
      type: "SET_TOTAL_ACCOUNT_WITHDRAWAL",
      totalAccountWithdraw: walletResponse.total_account_withdraw,
    })

    const accountResponse = await AccountService.getAll()
    dispatch({ type: "SET_ACCOUNTS", accounts: accountResponse.accounts })

    const promotionResponse = await PromotionService.getAll()
    console.log(promotionResponse)
    setPromotions(promotionResponse.promotion)
  }

  const navigate = (path) => {
    history.push(path)
    dispatch({ type: "SET_ACTIVE_MENU", path: "none" })
  }

  useEffect(() => {
    init()
    // eslint-disable-next-line
  }, [])

  // modal
  const [showReferral, setShowReferral] = useState(false)
  const toggleModal = () => {
    setShowReferral(!showReferral)
  }

  return (
    <>
      <Breadcrumb title="Dashboard" />
      <Container style={{ paddingBottom: 40 }}>
        <Row>
          <Col>
            {user.verification == null && (
              <Card>
                <CardBody className="p-4">
                  <ResponsiveCard>
                    <ResponsiveParagraph style={{ color: "red", fontWeight: "bold" }}>
                      Your profile isn’t verified. Please verify your profile to take full advantage
                      of our services!
                    </ResponsiveParagraph>
                    <ResponsiveButton
                      color="primary"
                      onClick={() => {
                        navigate(`${process.env.PUBLIC_URL}/account#verification`)
                      }}
                    >
                      Verify
                    </ResponsiveButton>
                  </ResponsiveCard>
                </CardBody>
              </Card>
            )}
            <Card>
              <CardBody className="p-4">
                <ResponsiveCard>
                  <ResponsiveParagraph>Invite friends and earn money</ResponsiveParagraph>
                  <ResponsiveButton
                    onClick={() => {
                      setShowReferral(true)
                    }}
                  >
                    Invite
                  </ResponsiveButton>
                </ResponsiveCard>
              </CardBody>
            </Card>
          </Col>
          {/* <Col>
            <DashboardCarousel />
          </Col> */}
        </Row>
        <Row>
          {promotions != null ? (
            promotions?.length > 0 &&
            promotions.map((promo) => {
              return (
                <Col key={promo.id}>
                  <Banner promotion={promo} />
                </Col>
              )
            })
          ) : (
            <Col>
              <Card>
                <CardBody style={{ textAlign: "center" }}>
                  <Spinner />
                </CardBody>
              </Card>
            </Col>
          )}
        </Row>
        <div className="mb-3" />
        <Row>
          <Col sm="12" lg="4" xl="3" className="project-list">
            <Card>
              <Row>
                <Col>
                  <Nav tabs className="border-tab">
                    <NavItem>
                      <NavLink
                        className={activeTab === "wallet" ? "active" : ""}
                        onClick={() => setActiveTab("wallet")}
                      >
                        <i
                          className="icofont icofont-wallet"
                          style={{
                            marginLeft: 5,
                            marginRight: 7,
                          }}
                        />
                        myWallet
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "partner" ? "active" : ""}
                        onClick={() => setActiveTab("partner")}
                      >
                        <i
                          className="fa fa-users"
                          style={{
                            marginLeft: 3,
                            marginRight: 7,
                          }}
                        />
                        Partner Account
                      </NavLink>
                    </NavItem>
                  </Nav>
                </Col>
              </Row>
            </Card>
          </Col>

          <Col sm="12" lg="8" xl="9">
            <TabContent activeTab={activeTab}>
              <TabPane tabId="wallet">
                <Wallet />
              </TabPane>
              <TabPane tabId="partner">
                <Partner />
              </TabPane>
            </TabContent>
          </Col>
        </Row>
      </Container>

      {showReferral && (
        <Modal isOpen={showReferral} backdrop={true} centered={true}>
          <ModalHeader toggle={toggleModal}>Invite Friends and Earn Money</ModalHeader>
          <ModalBody>
            <ReferralModal />
          </ModalBody>
        </Modal>
      )}
    </>
  )
}

export default Dashboard
