import React, { useState, useEffect } from 'react'
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Spinner,
  Modal,
  ModalHeader,
  ModalBody
} from 'reactstrap'
import { useSelector, useDispatch } from 'react-redux'

import PartnerSettings from '../../common/settings'
import MasterPassword from '../../common/settings/master_password'
import InvestorPassword from '../../common/settings/investor_password'
import ChangeLeverage from '../../common/settings/leverage'

import { ResponsiveDiv, StyledRow } from '../../common/components.style'

const settings = [
  {
    id: 1,
    title: 'Change Master Password',
    cta: <MasterPassword />
  },
  {
    id: 2,
    title: 'Change Investor Password',
    cta: <InvestorPassword />
  },
  {
    id: 3,
    title: 'Change Leverage',
    cta: <ChangeLeverage />
  }
]

const Partner = () => {
  const dispatch = useDispatch()
  const ibAccounts = useSelector((state) => {
    if (state.account.accounts?.length > 0) {
      return state.account.accounts.filter((e) => e.account_type === 2)
    }

    return []
  })
  const currencies = useSelector((state) => state.currency.currencies)
  const selectedAccount = useSelector((state) => state.dashboard.account)

  // modal
  const [showModal, setShowModal] = useState(false)
  const [selectedSetting, setSelectedSetting] = useState(null)
  const toggleModal = () => {
    setShowModal(!showModal)
  }

  useEffect(() => {
    if (selectedSetting != null && selectedAccount != null) {
      setShowModal(true)
    }
  }, [selectedSetting, selectedAccount])

  useEffect(() => {
    if (showModal === false) {
      setSelectedSetting(null)
      dispatch({
        type: 'SET_DASHBOARD_ACCOUNT',
        account: null
      })
    }
    // eslint-disable-next-line
  }, [showModal])

  return (
    <>
      {ibAccounts != null ? (
        ibAccounts?.length > 0 ? (
          ibAccounts.map((account) => {
            const currency = currencies.find((e) => {
              return e.id === account.currency_id
            })

            return (
              <Card className="card-absolute" key={account.id}>
                <CardHeader className="bg-success">
                  <h6 style={{ margin: 0 }}>
                    {`#${account.account_login} ${account.plan?.name} - LIVE`}
                  </h6>
                </CardHeader>
                <div className="card-right">
                  <ResponsiveDiv>
                    <PartnerSettings
                      settings={settings}
                      account={account}
                      setSelectedSetting={setSelectedSetting}
                    />
                  </ResponsiveDiv>
                </div>
                <CardBody className="text-center">
                  <StyledRow>
                    <Col sm={6} lg={4}>
                      <h6>{account.plan?.name}</h6>
                      <p>ACCOUNT TYPE</p>
                    </Col>
                    <Col sm={6} lg={4}>
                      <h6>1:{account.leverage}</h6>
                      <p>LEVERAGE</p>
                    </Col>
                    <Col sm={6} lg={4}>
                      <h6>{`${account.balance} ${currency?.name ?? ''}`}</h6>
                      <p>BALANCE</p>
                    </Col>
                    <Col sm={6} lg={4}>
                      <h6>{`${account.equity} ${currency?.name ?? ''}`}</h6>
                      <p>EQUITY</p>
                    </Col>
                    <Col sm={6} lg={4}>
                      <h6>{`${account.free_margin} ${currency?.name ?? ''}`}</h6>
                      <p>FREE MARGIN</p>
                    </Col>
                  </StyledRow>
                </CardBody>
              </Card>
            )
          })
        ) : (
          <Card>
            <CardBody className="text-center">No active accounts</CardBody>
          </Card>
        )
      ) : (
        <div style={{ textAlign: 'center' }}>
          <Spinner />
        </div>
      )}

      {selectedSetting !== null && selectedAccount !== null && (
        <Modal isOpen={showModal} backdrop={true} centered={true}>
          <ModalHeader toggle={toggleModal}>{settings[selectedSetting].title}</ModalHeader>
          <ModalBody>{settings[selectedSetting].cta}</ModalBody>
        </Modal>
      )}
    </>
  )
}

export default Partner
