import React from "react"
import { useHistory } from "react-router-dom"
import { useDispatch } from "react-redux"
import { Card, CardBody, Badge, CardHeader, CardFooter, Button } from "reactstrap"

const Banner = ({ promotion }) => {
  let history = useHistory()
  const dispatch = useDispatch()
  const navigate = (path) => {
    history.push(path)
    dispatch({ type: "SET_ACTIVE_MENU", path: path })
  }

  return (
    <Card>
      <CardHeader className="p-0">
        <img
          src={promotion.image_url}
          alt={promotion.name}
          style={{ backgroundColor: "white", width: "100%", borderRadius: "12px 12px 0 0" }}
        />
      </CardHeader>
      <CardBody className="p-4">
        <h5>{promotion.name}</h5>
        <p>{promotion.description}</p>
        <Badge color={promotion.available ? "success" : "danger"} pill>
          {promotion.available ? "Available" : "Unavailable"}
        </Badge>
      </CardBody>
      <CardFooter className="p-0">
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Button
            color="primary"
            style={{ borderRadius: "0 0 0 12px", width: "50%" }}
            onClick={() => {
              navigate(`${process.env.PUBLIC_URL}/financial#deposit`)
            }}
          >
            Proceed
          </Button>
          <a
            href={promotion.terms ?? "#"}
            target="_blank"
            rel="noopener noreferrer"
            style={{ display: "inline-block", width: "50%" }}
          >
            <Button style={{ borderRadius: "0 0 12px 0", width: "100%" }}>Terms & Conditions</Button>
          </a>
        </div>
      </CardFooter>
    </Card>
  )
}

export default Banner
