import React, { useEffect, useState } from "react"
import DataTable from "react-data-table-component"
import moment from "moment"
import { useSelector } from "react-redux"
import { Spinner } from "reactstrap"

import AccountService from "../../../network/services/account"

const columns = [
  {
    name: "Symbol",
    selector: "Symbol",
    sortable: true,
  },
  {
    name: "Ticket",
    selector: "Order",
    sortable: true,
  },
  {
    name: "Time",
    selector: "Time",
    sortable: true,
    width: "200px",
    format: (row) => {
      return moment.unix(row.Time).format("yyyy-MM-DD hh:mmA")
    },
  },
  // {
  //   name: "Type",
  //   selector: "type",
  //   sortable: true,
  // },
  {
    name: "Volume",
    selector: "VolumeClosed",
    sortable: true,
    format: (row) => {
      return Number.parseInt(row.VolumeClosed) / 10000
    },
  },
  {
    name: "PriceClose",
    selector: "Price",
    sortable: true,
  },
  {
    name: "S / L",
    selector: "PriceSL",
    sortable: true,
  },
  {
    name: "T / P",
    selector: "PriceTP",
    sortable: true,
  },
  {
    name: "PriceCurrent",
    selector: "PricePosition",
    sortable: true,
  },
  {
    name: "Swap",
    selector: "Storage",
    sortable: true,
  },
  {
    name: "Profit",
    selector: "Profit",
    sortable: true,
  },
]

const MonitoringClosedTable = () => {
  const [isBusy, setIsBusy] = useState(false)
  const [data, setData] = useState(null)
  const [total, setTotal] = useState(0)
  const selectedAccount = useSelector((state) => state.monitoring.account)

  const fetchClosedOrders = async () => {
    try {
      if (selectedAccount != null) {
        setIsBusy(true)
        const result = await AccountService.getClosedOrders(selectedAccount.id)
        console.log(result)

        setTotal(result.count)
        setData(result.orders)
        setIsBusy(false)
      }
    } catch (error) {
      console.log(error)
      setIsBusy(false)
    }
  }

  const handlePageChange = async (page) => {
    setIsBusy(true)
    const result = await AccountService.getClosedOrders(selectedAccount.id, {
      offset: (page - 1) * 10,
    })
    setData(result?.transactions?.data ?? [])
    setIsBusy(false)
  }

  useEffect(() => {
    fetchClosedOrders()
    // eslint-disable-next-line
  }, [selectedAccount])

  return (
    <>
      <h5>Closed Orders</h5>
      {
        data != null ? data.length > 0 ? (
          <DataTable
            noHeader
            data={data}
            columns={columns}
            striped={true}
            center={true}
            pagination
            paginationComponentOptions={{ noRowsPerPage: true }}
            paginationServer
            paginationTotalRows={total}
            onChangePage={handlePageChange}
            progressPending={isBusy}
          />
        ) : (
            <h6>No records found</h6>
          ) : (
            <div style={{ textAlign: "center" }}>
              <Spinner />
            </div>
          )
      }
    </>
  )
}

export default MonitoringClosedTable
