import React, { useState, useEffect } from "react"
import { Card, CardHeader, CardBody, Col, Table, Row } from "reactstrap"
import moment from "moment"
import { useSelector } from "react-redux"

import AccountService from "../../../network/services/account"

const MonitoringGraphical = () => {
  const [currency, setCurrency] = useState()
  const [mt5, setMt5] = useState({})
  const [count, setCount] = useState(0)
  const [profit, setProfit] = useState(0)
  const selectedAccount = useSelector((state) => state.monitoring.account)
  const currencies = useSelector((state) => state.currency.currencies)

  const fetchAccount = async (id) => {
    const result = await AccountService.get(id)
    console.log(result)
    if (result.mt5) {
      setMt5(result.mt5)
    }
  }

  const fetchSummary = async (id) => {
    const result = await AccountService.getOpenOrdersCount(id)
    const profit = await AccountService.getAccountProfit(id)
    setCount(result.count)
    setProfit(profit.total)
  }

  useEffect(() => {
    let fCurrency = currencies.find((e) => e.id === selectedAccount.currency_id)
    setCurrency(fCurrency)

    fetchAccount(selectedAccount.id)
    // eslint-disable-next-line
  }, [selectedAccount])

  useEffect(() => {
    fetchSummary(selectedAccount.id)
    // eslint-disable-next-line
  }, [])

  return (
    <Row>
      <Col lg={6}>
        <Card>
          <CardHeader style={{ padding: 20 }} className="bg-primary">
            <h5 className="text-center">Account stats</h5>
          </CardHeader>
          <CardBody style={{ padding: 16 }}>
            <Table borderless>
              <tbody>
                <tr>
                  <td className="text-right" width="50%">
                    Current Balance, {currency?.name} :
                  </td>
                  <td>{mt5?.Balance ?? "-"}</td>
                </tr>
                <tr>
                  <td className="text-right">Current Equity, {currency?.name} :</td>
                  <td>{mt5?.EquityPrevDay ?? "-"}</td>
                </tr>
                <tr>
                  <td className="text-right">Total Account Profitability :</td>
                  <td>{profit ?? "-"}</td>
                </tr>
                <tr>
                  <td className="text-right">Open Orders Count :</td>
                  <td>{count ?? "-"}</td>
                </tr>
              </tbody>
            </Table>
          </CardBody>
        </Card>
      </Col>
      <Col lg={6}>
        <Card>
          <CardHeader style={{ padding: 20 }} className="bg-primary">
            <h5 className="text-center">Account data</h5>
          </CardHeader>
          <CardBody style={{ padding: 16 }}>
            <Table borderless>
              <tbody>
                <tr>
                  <td className="text-right" width="50%">
                    ACCOUNT :
                  </td>
                  <td>{selectedAccount?.account_login ?? "-"}</td>
                </tr>
                <tr>
                  <td className="text-right">ACCOUNT HOLDER :</td>
                  <td>{mt5?.Name ?? "-"}</td>
                </tr>
                <tr>
                  <td className="text-right">REGISTRATION DATE :</td>
                  <td>
                    {mt5?.Registration != null
                      ? moment.unix(mt5.Registration).format("yyyy-MM-DD")
                      : "-"}
                  </td>
                </tr>
                <tr>
                  <td className="text-right">ACCOUNT TYPE :</td>
                  <td>{selectedAccount?.account_type === 0 ? "Live" : "Demo"}</td>
                </tr>
                <tr>
                  <td className="text-right">COUNTRY :</td>
                  <td>{mt5?.Country ?? "-"}</td>
                </tr>
                <tr>
                  <td className="text-right">CURRENCY :</td>
                  <td>{currency?.name ?? "-"}</td>
                </tr>
                <tr>
                  <td className="text-right">LEVERAGE :</td>
                  <td>1:{mt5?.Leverage ?? "-"}</td>
                </tr>
              </tbody>
            </Table>
          </CardBody>
        </Card>
      </Col>
    </Row>
  )
}

export default MonitoringGraphical
