import React, { useEffect, useState } from "react"
import { Row, Col, Form, FormGroup, Input, Button } from "reactstrap"
import { useForm, Controller } from "react-hook-form"
import { toast } from "react-toastify"
import PhoneInput from "react-phone-input-2"
import "react-phone-input-2/lib/style.css"

import AuthService from "../../network/services/auth"
import UserService from "../../network/services/user"

const SignUpForm = ({ toggle, referralCode }) => {
  const { control, register, handleSubmit, errors, reset, watch, clearErrors, setError } = useForm()
  const watchPassword = watch("password")

  const [email, setEmail] = useState("")
  const [isEmailVerified, setIsEmailVerified] = useState(false)

  const [generalError, setGeneralError] = useState(null)

  const submitRegistration = async (data) => {
    try {
      console.log(data)

      let postData = {
        first_name: data.first_name,
        last_name: data.last_name,
        email: email,
        mobile: data.mobile,
        auth_code: data.auth_code,
        password: data.password,
        password_confirmation: data.confirm_password,
      }
      
      if (referralCode != null) {
        postData["referral_code"] = referralCode
      }
      const result = await AuthService.register(postData)

      if (result?.user?.id) {
        toast.success("Register successfully!", {
          position: toast.POSITION.TOP_RIGHT,
        })
        toggle()
        resetForm()
      }
    } catch (error) {
      console.log(error)
      setGeneralError(error.message)
    }
  }

  const verifyEmail = async () => {
    if (email == null || email === "") {
      setError("email", {
        type: "manual",
        message: "Email is required",
      })
      return
    }

    try {
      const result = await UserService.verifyEmail({
        email: email,
      })

      if (result.success) {
        const codeResult = await AuthService.sendVerification({
          email: email,
        })

        if (codeResult.success) {
          setIsEmailVerified(true)
          clearErrors("email")
        } else {
          setError("email", {
            type: "manual",
            message: codeResult.message ?? "Please try again later",
          })
          throw codeResult
        }
      } else {
        console.log("?")
        setError("email", {
          type: "manual",
          message: "Email already taken",
        })
        throw result
      }
    } catch (error) {
      console.log(error)
    }
  }

  const resetForm = () => {
    reset()
    setEmail("")
    setGeneralError(null)
    setIsEmailVerified(false)
  }

  return (
    <Form
      className="theme-form"
      onSubmit={handleSubmit(submitRegistration)}
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
      }}
    >
      <div className="placeholder-height" />
      <h4 className="text-center">REGISTER</h4>
      <FormGroup>
        <Input
          className="form-control"
          type="text"
          placeholder="Email"
          name="email"
          value={email}
          onChange={(event) => {
            setEmail(event.target.value)
          }}
          disabled={isEmailVerified ? true : false}
          innerRef={register({ required: "Email is required" })}
        />
        <span style={{ color: "red" }}>{errors.email && errors.email.message}</span>
      </FormGroup>

      {isEmailVerified ? (
        <>
          <Row form>
            <Col md="12">
              <FormGroup>
                <span
                  style={{ color: "green" }}
                >{`Please check ${email} for verification code`}</span>
                <Input
                  className="form-control"
                  type="text"
                  placeholder="Verification Code"
                  name="auth_code"
                  innerRef={register({ required: "Verification Code is required" })}
                />
                <span style={{ color: "red" }}>{errors.auth_code && errors.auth_code.message}</span>
              </FormGroup>
            </Col>
            <Col md="6">
              <FormGroup>
                <Input
                  className="form-control"
                  type="text"
                  placeholder="First Name"
                  name="first_name"
                  innerRef={register({ required: "First name is required" })}
                />
                <span style={{ color: "red" }}>
                  {errors.first_name && errors.first_name.message}
                </span>
              </FormGroup>
            </Col>
            <Col md="6">
              <FormGroup>
                <Input
                  className="form-control"
                  type="text"
                  placeholder="Last Name"
                  name="last_name"
                  innerRef={register({ required: "Last name is required" })}
                />
                <span style={{ color: "red" }}>{errors.last_name && errors.last_name.message}</span>
              </FormGroup>
            </Col>
          </Row>
          <FormGroup>
            <Controller
              name="mobile"
              control={control}
              rules={{ required: "Mobile is required" }}
              as={
                <PhoneInput
                  country={"my"}
                  className="form-control"
                  inputStyle={{ width: "100%" }}
                  dropdownStyle={{
                    maxHeight: 100,
                  }}
                />
              }
            />
            {/* <PhoneInput value={mobile}  /> */}
            <span style={{ color: "red" }}>{errors.mobile && errors.mobile.message}</span>
          </FormGroup>
          <FormGroup>
            <Input
              className="form-control"
              type="password"
              placeholder="Password"
              name="password"
              innerRef={register({
                required: "Password is required",
                minLength: {
                  value: 8,
                  message: "Password minumum length is 8",
                },
              })}
            />
            <span style={{ color: "red" }}>{errors.password && errors.password.message}</span>
          </FormGroup>
          <FormGroup>
            <Input
              className="form-control"
              type="password"
              placeholder="Confirm Password"
              name="confirm_password"
              innerRef={register({
                required: "Confirm Password is required",
                minLength: 8,
                validate: (value) => {
                  return value === watchPassword || "Password does not match"
                },
              })}
            />
            <span style={{ color: "red" }}>
              {errors.confirm_password && errors.confirm_password.message}
            </span>
          </FormGroup>
          <Row form>
            <Col>
              <Button color="primary btn-block" type="submit">
                Sign Up
              </Button>
            </Col>
          </Row>
          <span style={{ color: "red" }}>{generalError != null && generalError}</span>
        </>
      ) : (
        <Row form>
          <Col>
            <Button
              color="primary btn-block"
              onClick={() => {
                verifyEmail()
              }}
            >
              Send verification code
            </Button>
          </Col>
        </Row>
      )}
    </Form>
  )
}

export default SignUpForm
