import React from "react"
import { Card, CardBody } from "reactstrap"
import TransferForm from "./form"

const MT5Transfer = () => {
  return (
    <Card>
      <CardBody>
        <TransferForm />
      </CardBody>
    </Card>
  )
}

export default MT5Transfer
