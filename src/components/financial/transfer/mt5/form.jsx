import React, { useState, useEffect } from 'react'
import { Row, Col, Form, FormGroup, Label, Input, Button, Spinner } from 'reactstrap'
import { useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import { toast } from 'react-toastify'
import SweetAlert from 'sweetalert2'

import AccountService from '../../../../network/services/account'
import TransferService from '../../../../network/services/transfer'

const TransferForm = () => {
  const dispatch = useDispatch()
  const accounts = useSelector((state) => state.account.accounts)

  const [fromAccounts, setFromAccounts] = useState([])
  const [toAccounts, setToAccounts] = useState([])
  const [fromSelected, setFromSelected] = useState(null)
  const [toSelected, setToSelected] = useState(null)

  // form
  const { register, handleSubmit, errors, reset } = useForm()
  const handleTransfer = async (data) => {
    SweetAlert.fire({
      title: 'Are you sure you want to transfer?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirm',
      cancelButtonText: 'Cancel',
      reverseButtons: true
    }).then(async (result) => {
      if (result.value) {
        try {
          if (data !== '') {
            const result = await TransferService.createMt5Transfer(data)
            console.log(result)

            // success
            if (result?.account_transfer?.id) {
              toast.success('Transfer Success', {
                position: toast.POSITION.TOP_RIGHT
              })

              reset()
              window.location.reload()
            }
          } else {
            errors.showMessages()
          }
        } catch (error) {
          toast.error(error.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      }
    })
  }

  const init = async () => {
    // live accounts only
    const accountResponse = await AccountService.getAll({ filters: { account_type: 0 } })
    console.log(accountResponse)
    dispatch({ type: 'SET_ACCOUNTS', accounts: accountResponse.accounts })
  }

  useEffect(() => {
    init()
    // eslint-disable-next-line
  }, [])

  useEffect(() => {
    if (accounts?.length > 1 && fromSelected == null && toSelected == null) {
      setFromSelected(accounts[0].id)
      setToSelected(accounts[1].id)
    }
  }, [accounts])

  useEffect(() => {
    if (accounts?.length > 1) {
      setToAccounts(accounts.filter((account) => account.id != fromSelected))
    }
  }, [fromSelected])

  useEffect(() => {
    if (accounts?.length > 1) {
      setFromAccounts(accounts.filter((account) => account.id != toSelected))
    }
  }, [toSelected])

  if (accounts != null) {
    if (accounts?.length > 1) {
      return (
        <Form className="theme-form" onSubmit={handleSubmit(handleTransfer)}>
          <Row>
            <Col>
              <FormGroup>
                <Label>From account</Label>
                <Input
                  type="select"
                  name="from_account_id"
                  className="form-control digits"
                  innerRef={register({ required: true })}
                  defaultValue={fromSelected}
                >
                  {fromAccounts?.length > 0 &&
                    fromAccounts.map((account) => {
                      return (
                        <option
                          key={account.id}
                          value={account.id}
                        >{`${account.account_login} - Available Balance: ${account.balance}`}</option>
                      )
                    })}
                </Input>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Label>To account</Label>
                <Input
                  type="select"
                  name="to_account_id"
                  className="form-control digits"
                  innerRef={register({ required: true })}
                  defaultValue={toSelected}
                >
                  {toAccounts?.length > 0 &&
                    toAccounts.map((account) => {
                      return (
                        <option
                          key={account.id}
                          value={account.id}
                        >{`${account.account_login} - Available Balance: ${account.balance}`}</option>
                      )
                    })}
                </Input>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col>
              <FormGroup>
                <Label>Transfer Amount</Label>
                <Input
                  className="form-control"
                  type="number"
                  name="amount"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: 'red' }}>{errors.amount && 'Amount is required'}</span>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col>
              <FormGroup className="mb-0">
                <Button color="success" className="mr-3 pull-right">
                  Confirm
                </Button>
              </FormGroup>
            </Col>
          </Row>
        </Form>
      )
    } else {
      return <p>Need a minimum of 2 accounts to enable internal transfer</p>
    }
  }

  return (
    <div style={{ textAlign: 'center' }}>
      <Spinner />
    </div>
  )
}

export default TransferForm
