import React, { useEffect, useState } from "react"
import WalletOption from "../options"
import WalletTransfer from "./wallet"
import MT5Transfer from "./mt5"

const Transfer = ({ refresh }) => {
  const [option, setOption] = useState()

  // reset options on tab change
  useEffect(() => {
    setOption(null)
  }, [refresh])

  return (
    <>
      {option == null ? (
        <WalletOption setOption={setOption} type="transfer" />
      ) : (
        <>
          {option === 1 ? (
            <WalletTransfer setOption={setOption} />
          ) : (
            <MT5Transfer setOption={setOption} />
          )}
        </>
      )}
    </>
  )
}

export default Transfer
