import React, { useEffect } from 'react'
import { Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap'
import { useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import { toast } from 'react-toastify'
import SweetAlert from "sweetalert2"

import WalletService from '../../../../network/services/wallet'
import TransferService from '../../../../network/services/transfer'

const TransferForm = () => {
  const dispatch = useDispatch()
  const wallets = useSelector((state) => state.wallet.wallets)
  const { register, handleSubmit, errors, reset } = useForm()

  const handleTransfer = async (data) => {
    SweetAlert.fire({
      title: 'Are you sure you want to transfer?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirm',
      cancelButtonText: 'Cancel',
      reverseButtons: true
    }).then(async (result) => {
      if (result.value) {
        try {
          if (data !== '') {
            const result = await TransferService.create(data)
            console.log(result)

            // success
            if (result?.transfer?.id) {
              toast.success('Transfer Success', {
                position: toast.POSITION.TOP_RIGHT
              })

              reset()
              window.location.reload()
            }
          } else {
            errors.showMessages()
          }
        } catch (error) {
          toast.error(error.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      }
    })
  }

  const init = async () => {
    const walletResponse = await WalletService.get()
    console.log(walletResponse)
    dispatch({ type: 'SET_WALLETS', wallets: walletResponse.wallets })
  }

  useEffect(() => {
    init()
    // eslint-disable-next-line
  }, [])

  return (
    <Form className="theme-form" onSubmit={handleSubmit(handleTransfer)}>
      <Row>
        <Col>
          <FormGroup>
            <Label>Wallet Currency</Label>
            <Input
              type="select"
              name="currency_id"
              className="form-control digits"
              innerRef={register({ required: true })}
            >
              {wallets?.length > 0 &&
                wallets.map((wallet) => {
                  return (
                    <option
                      key={wallet.id}
                      value={wallet.currency_id}
                    >{`${wallet.currency?.name} Available Balance: ${wallet.balance}`}</option>
                  )
                })}
            </Input>
          </FormGroup>
        </Col>
      </Row>
      {/* <Row>
        <Col>
          <FormGroup>
            <Label>Client Area Password</Label>
            <Input
              className="form-control"
              type="password"
              name="password"
              innerRef={register({ required: true })}
            />
            <span style={{ color: "red" }}>{errors.password && "Password is required"}</span>
          </FormGroup>
        </Col>
      </Row> */}
      <Row>
        <Col>
          <FormGroup>
            <Label>Transfer Amount</Label>
            <Input
              className="form-control"
              type="number"
              name="amount"
              innerRef={register({ required: true })}
            />
            <span style={{ color: 'red' }}>{errors.amount && 'Amount is required'}</span>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label>To Account (Email)</Label>
            <Input
              className="form-control"
              type="email"
              name="to_email"
              innerRef={register({ required: true })}
            />
            <span style={{ color: 'red' }}>{errors.receiver && 'Email is required'}</span>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup className="mb-0">
            <Button color="success" className="mr-3 pull-right">
              Confirm
            </Button>
          </FormGroup>
        </Col>
      </Row>
    </Form>
  )
}

export default TransferForm
