import React from "react"
import { Card, CardBody, Button } from "reactstrap"
import TransferForm from "./form"

const WalletTransfer = ({ setOption }) => {
  return (
    <>
      <Card>
        <CardBody>
          <TransferForm />
        </CardBody>
      </Card>

      <Button
        color="primary"
        onClick={() => {
          setOption(null)
        }}
      >
        Back
      </Button>

      {/* Add gap */}
      <div style={{ padding: 24, clear: "both" }} />
    </>
  )
}

export default WalletTransfer
