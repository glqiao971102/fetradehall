import React, { useEffect } from "react"
import { Row, Col } from "reactstrap"
import { useSelector, useDispatch } from "react-redux"

import {
  HoverCard,
  ResponsiveCard,
  ResponsiveIcon,
  ResponsiveImage,
  ResponsiveDiv,
} from "../../components.style"

const SelectPaymentMethod = () => {
  const dispatch = useDispatch()
  const payments = useSelector((state) => state.payment.payments)
  const deposit = useSelector((state) => state.deposit)

  useEffect(() => {
    // set default payments id
    if (payments?.length > 0 && deposit.wallet.method == null) {
      dispatch({ type: "SET_DEPOSIT_WALLET_METHOD", method: payments[0].id })
    }
    // eslint-disable-next-line
  }, [payments])

  return (
    <>
      <h5>Select Payment Method</h5>

      <Row>
        {payments?.length > 0 &&
          payments.map((payment, index) => {
            return (
              <Col sm="12" lg="6" key={payment.id}>
                <HoverCard
                  isSelected={deposit?.wallet?.method === payment.id ? "#f9b600" : null}
                  onClick={() => {
                    dispatch({ type: "SET_DEPOSIT_WALLET_METHOD", method: payment.id })
                  }}
                >
                  <ResponsiveCard>
                    {payment.name?.toLowerCase() === "bank transfer" ? (
                      <ResponsiveIcon className="icofont icofont-bank" />
                    ) : (
                      payment.name?.toLowerCase() === "usdt" && (
                        <ResponsiveImage src="/tether.png" alt="tether" />
                      )
                    )}
                    <ResponsiveDiv>
                      <h6>{`${payment.name}`}</h6>
                      <p>{`${payment.funding_time} with ${payment.fee} fee`}</p>
                    </ResponsiveDiv>
                  </ResponsiveCard>
                </HoverCard>
              </Col>
            )
          })}
      </Row>
    </>
  )
}

export default SelectPaymentMethod
