import React, { useEffect, useState } from 'react'
import { Row, Form, Col, Card, CardBody, Button, Modal, ModalHeader, ModalBody } from 'reactstrap'
import { useForm, FormProvider } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import StepZilla from 'react-stepzilla'
import QRCode from 'react-qr-code'
import { toast } from 'react-toastify'
import SweetAlert from 'sweetalert2'

import SelectCurrency from './currency'
import SelectPaymentMethod from './payment'
import ChooseAmount from './amount'
import WalletService from '../../../../network/services/wallet'
import PaymentService from '../../../../network/services/payment'

const WalletDeposit = ({ setOption }) => {
  const dispatch = useDispatch()
  const deposit = useSelector((state) => state.deposit)
  const methods = useForm()
  const [stage, setStage] = useState(0)

  // modal
  const [showModal, setShowModal] = useState(false)
  const [qrCode, setQrCode] = useState(null)
  const toggle = () => {
    if (showModal === true) {
      methods.reset()
    }

    setShowModal(!showModal)
  }

  useEffect(() => {
    // user close modal
    if (showModal == false && qrCode != null) {
      window.location.reload()
    }
  }, [showModal])

  // use redux for payment_id & wallet_id
  // because form doesn't work with stepzilla
  const handleDeposit = async (data) => {
    SweetAlert.fire({
      title: 'Are you sure you want to deposit?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirm',
      cancelButtonText: 'Cancel',
      reverseButtons: true
    }).then(async (result) => {
      if (result.value) {
        try {
          data['wallet_id'] = deposit.wallet.id
          data['payment_id'] = deposit.wallet.method
          console.log(data)

          if (data !== '') {
            let result = await WalletService.deposit(data)
            console.log(result)

            try {
              // awepay
              if (deposit.wallet.method === 1) {
                document.write(result.body_html)
              }

              // bluepay
              if (deposit.wallet.method === 2) {
                setQrCode(result.body?.paymentAddr)
                setShowModal(true)
              }
            } catch (error) {
              console.log(error)
            }
          } else {
            methods.errors.showMessages()
          }
        } catch (error) {
          toast.error(error.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      }
    })
  }

  const init = async () => {
    const walletResponse = await WalletService.get()
    console.log(walletResponse)
    dispatch({ type: 'SET_WALLETS', wallets: walletResponse.wallets })

    const paymentResponse = await PaymentService.getAll()
    console.log(paymentResponse)
    dispatch({ type: 'SET_PAYMENTS', payments: paymentResponse.payments })
  }

  useEffect(() => {
    init()
    // eslint-disable-next-line
  }, [])

  return (
    <>
      <Card>
        <CardBody>
          <Row>
            <Col
              sm={12}
              md={4}
              className={`u-pearl ${stage === 0 && 'current'} ${stage > 0 && 'done'}`}
            >
              <span className="u-pearl-number">1</span>
              <span className="u-pearl-title">Select Currency</span>
            </Col>
            <Col
              sm={12}
              md={4}
              className={`u-pearl ${stage === 1 && 'current'} ${stage > 1 && 'done'}`}
            >
              <span className="u-pearl-number">2</span>
              <span className="u-pearl-title">Select Payment Method</span>
            </Col>
            <Col sm={12} md={4} className={`u-pearl ${stage === 2 && 'current'}`}>
              <span className="u-pearl-number">3</span>
              <span className="u-pearl-title">Choose Amount</span>
            </Col>
          </Row>
        </CardBody>
      </Card>

      <FormProvider {...methods}>
        <Form className="theme-form" onSubmit={methods.handleSubmit(handleDeposit)}>
          <StepZilla
            steps={[
              {
                name: 'Step 1',
                component: <SelectCurrency />
              },
              {
                name: 'Step 2',
                component: <SelectPaymentMethod />
              },
              {
                name: 'Step 3',
                component: <ChooseAmount />
              }
            ]}
            showSteps={false}
            onStepChange={(index) => {
              setStage(index)
            }}
          />
        </Form>
      </FormProvider>

      {stage === 0 && (
        <Button
          color="primary"
          onClick={() => {
            setOption(null)
          }}
        >
          Back
        </Button>
      )}

      <Modal isOpen={showModal} backdrop={true} centered={true}>
        <ModalHeader toggle={toggle}>Please deposit to the address below</ModalHeader>
        <ModalBody>
          <div style={{ textAlign: 'center' }}>
            <p>{qrCode}</p>
            {qrCode && <QRCode value={qrCode} />}
          </div>
        </ModalBody>
      </Modal>

      {/* Add gap */}
      <div style={{ padding: 24, clear: 'both' }} />
    </>
  )
}

export default WalletDeposit
