import React from "react"
import { Row, Col, Card, CardHeader, CardBody, FormGroup, Label, Input, Button } from "reactstrap"
import { useFormContext } from "react-hook-form"
import { useSelector } from "react-redux"

const ChooseAmount = () => {
  const deposit = useSelector((state) => state.deposit)

  // form
  const { register, errors, formState } = useFormContext()
  const { isSubmitting } = formState

  return (
    <>
      <Card>
        <CardHeader>
          <h5>Choose Amount</h5>
        </CardHeader>
        <CardBody>
          <Row>
            <Col>
              <FormGroup>
                <Label>
                  Deposit Amount
                  {deposit?.wallet?.method === 2 && (
                    <span style={{ color: "green" }}>{` (Minimum deposit of 10USD)`}</span>
                  )}
                </Label>
                <Input
                  className="form-control"
                  type="number"
                  name="amount"
                  min="10"
                  innerRef={register({
                    required: "Amount is required",
                    min: {
                      value: 10,
                      message: "Minimum deposit of 10USD",
                    },
                  })}
                />
                <span style={{ color: "red" }}>{errors.amount && errors.amount.message}</span>
                <p />
                {deposit?.wallet?.method === 1 && (
                  <span style={{ color: "red" }}>
                    Warning: Make sure Your card/account have the enough balance and all the details
                    are valid. Three continuous transactions failure could block your card.
                  </span>
                )}
                {deposit?.wallet?.method === 2 && (
                  <span style={{ color: "green" }}>Rate of 1 USD to 0.97 USDT</span>
                )}
              </FormGroup>
              <FormGroup>
                <div className="custom-control custom-checkbox mb-3">
                  <Input
                    className="custom-control-input"
                    id="terms_validation"
                    type="checkbox"
                    required
                  />
                  <Label className="custom-control-label" htmlFor="terms_validation">
                    I have read all instructions and agree with terms and conditions of payments
                    operations
                  </Label>
                  <div className="invalid-feedback">Please agree to the terms</div>
                </div>
              </FormGroup>
              <Button
                color="primary btn-block"
                type="submit"
                style={{ maxWidth: 150, float: "right" }}
              >
                {isSubmitting ? "Loading..." : "Submit"}
              </Button>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </>
  )
}

export default ChooseAmount
