import React, { useEffect } from "react"
import { Row, Col } from "reactstrap"
import { useSelector, useDispatch } from "react-redux"

import { HoverCard, ResponsiveCard, ResponsiveIcon, ResponsiveDiv } from "../../components.style"

const SelectCurrency = () => {
  const dispatch = useDispatch()
  const wallets = useSelector((state) => state.wallet.wallets)
  const deposit = useSelector((state) => state.deposit)

  useEffect(() => {
    if (wallets?.length > 0) {
      dispatch({ type: "SET_DEPOSIT_MT5_WALLET", wallet: wallets[0].id })
    }
    // eslint-disable-next-line
  }, [])

  return (
    <>
      <h5>Select Currency</h5>
      <Row>
        {wallets?.length > 0 &&
          wallets.map((wallet) => {
            return (
              <Col sm="12" lg="6" key={wallet.id}>
                <HoverCard
                  isSelected={deposit?.mt5?.wallet === wallet.id ? "#f9b600" : null}
                  onClick={() => {
                    dispatch({ type: "SET_DEPOSIT_MT5_WALLET", wallet: wallet.id })
                  }}
                >
                  <ResponsiveCard>
                    <ResponsiveIcon className="icofont icofont-cur-dollar" />
                    <ResponsiveDiv>
                      <h6>{`${wallet.currency?.name}`}</h6>
                      <p>{`Available Balance in wallet: ${wallet.balance}`}</p>
                    </ResponsiveDiv>
                  </ResponsiveCard>
                </HoverCard>
              </Col>
            )
          })}
      </Row>
    </>
  )
}

export default SelectCurrency
