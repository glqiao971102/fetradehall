import React, { useEffect, useState, useRef } from "react"
import DataTable from "react-data-table-component"
import moment from "moment"
import { Row, Col, Card, CardHeader, Input } from "reactstrap"

import AccountService from "../../../../network/services/account"
import HistoryFilters from "./filters"

const columns = [
  {
    name: "Ticket",
    selector: "id",
    sortable: true,
  },
  {
    name: "Date",
    selector: "created_at",
    sortable: true,
    width: "200px",
    format: (row) => {
      return moment(row.created_at).format("yyyy-MM-DD hh:mmA")
    },
  },
  {
    name: "Type",
    selector: "txn_type",
    sortable: true,
    format: (row) => {
      switch (row.txn_type) {
        case 0:
          return "Deposit"
        case 1:
          return "Withdraw"
        case 2:
          return "Transfer"
        default:
          return "-"
      }
    },
  },
  {
    name: "Account Number",
    selector: "account.account_login",
    sortable: true,
    format: (row) => {
      return row?.account?.account_login ?? "-"
    },
  },
  {
    name: "Wallet",
    selector: "wallet.id",
    sortable: true,
    format: (row) => {
      return row?.wallet?.id ?? "-"
    },
  },
  {
    name: "Currency",
    selector: "currency_unit",
    sortable: true,
  },
  {
    name: "Amount",
    selector: "amount",
    sortable: true,
    right: true,
    format: (row) => {
      return row.debit_amount > 0 ? row.debit_amount : row.credit_amount
    },
  },
  {
    name: "Status",
    selector: "status",
    sortable: true,
    format: (row) => {
      switch (row.status) {
        case 0:
          return "Pending"
        case 1:
          return "Success"
        case 2:
          return "Failed"
        default:
          return "-"
      }
    },
  },
]

const types = [
  {
    id: -1,
    name: "All",
  },
  {
    id: 0,
    name: "Deposit",
  },
  {
    id: 1,
    name: "Withdraw",
  },
  {
    id: 2,
    name: "Transfer",
  },
]

const HistoryTable = () => {
  const [isBusy, setIsBusy] = useState(false)
  const [total, setTotal] = useState(0)
  const [data, setData] = useState([])
  const [sidebaron, setSidebaron] = useState(true)
  const [filters, setFilters] = useState(null)
  const [searchKeyword, setSearchKeyword] = useState("")
  const wrapperRef = useRef()

  const handlePageChange = async (page) => {
    setIsBusy(true)
    const result = await AccountService.getTransactions({ page })
    setData(result.account_transactions.data)
    setIsBusy(false)
  }

  const handleFilter = async () => {
    setIsBusy(true)
    const result = await AccountService.getTransactions({
      filters,
    })
    setData(result.account_transactions.data)
    setIsBusy(false)
  }

  const init = async () => {
    setIsBusy(true)
    const result = await AccountService.getTransactions()
    console.log(result)

    setTotal(result.account_transactions?.meta?.total ?? 0)
    setData(result.account_transactions?.data ?? [])
    setIsBusy(false)
  }

  const onClickFilter = () => {
    if (sidebaron) {
      setSidebaron(false)
      wrapperRef.current.classList.add("sidebaron")
    } else {
      setSidebaron(true)
      wrapperRef.current.classList.remove("sidebaron")
    }
  }

  const handleSearchKeyword = (keyword) => {
    setSearchKeyword(keyword)
    setFilters({
      account_login: keyword,
    })
  }

  useEffect(() => {
    init()
  }, [])

  useEffect(() => {
    handleFilter()
  }, [filters])

  return (
    <div className="product-wrapper" ref={wrapperRef}>
      <div className="product-grid" style={{ minHeight: "50vh" }}>
        <Row>
          <Col xl="3">
            <div className="product-sidebar">
              <div className="filter-section">
                <Card className="m-0">
                  <CardHeader>
                    <h6 className="mb-0 f-w-600">
                      Filters
                      <span className="pull-right">
                        <i className="fa fa-chevron-down toggle-data" onClick={onClickFilter}></i>
                      </span>
                    </h6>
                  </CardHeader>
                  <div className="left-filter">
                    <div style={{ padding: 16 }} />
                    <HistoryFilters types={types} handleFilter={setFilters} />
                  </div>
                </Card>
              </div>
            </div>
          </Col>
          <Col xl="9" sm="12">
            <div style={{ height: "100%", display: "flex", alignItems: "center" }}>
              <Input
                className="form-control"
                type="text"
                placeholder="Search Account Number"
                defaultValue={searchKeyword}
                onChange={(e) => handleSearchKeyword(e.target.value)}
              />
            </div>
          </Col>
        </Row>

        <div className="product-wrapper-grid" style={{ marginTop: 16 }}>
          <DataTable
            noHeader
            data={data}
            columns={columns}
            striped={true}
            center={true}
            pagination
            paginationComponentOptions={{ noRowsPerPage: true }}
            paginationServer
            paginationTotalRows={total}
            onChangePage={handlePageChange}
            isBusy={isBusy}
          />
        </div>
      </div>
    </div>
  )
}

export default HistoryTable
