import React, { useEffect, useState } from "react"
import { Container, Row, Col, Card, Nav, NavItem, NavLink, TabContent, TabPane } from "reactstrap"
import { useLocation, useHistory } from "react-router-dom"
import { isEmpty } from "lodash"

import Breadcrumb from "../../layout/breadcrumb"
import Deposit from "./deposit"
import Withdrawal from "./withdrawal"
import History from "./history"
import Bonus from "./bonus"
import Transfer from "./transfer"

const tabs = {
  deposit: "Deposit",
  withdrawal: "Withdrawal",
  transfer: "Wallet Transfer",
  history: "Deposit/Withdrawal History",
  bonus: "Manage Bonus",
}

const Financial = () => {
  const [activeTab, setActiveTab] = useState("deposit")
  const [refresh, setRefresh] = useState(null)
  let location = useLocation()
  let history = useHistory()

  useEffect(() => {
    if (!isEmpty(location.hash)) {
      setActiveTab(location.hash.replace("#", ""))
    } else {
      setActiveTab("deposit")
    }
  }, [location])

  useEffect(() => {
    setRefresh(Math.random())
  }, [activeTab])

  const setTab = (val) => {
    history.push("#" + val)
  }

  return (
    <>
      <Breadcrumb parent="Financial" title={tabs[activeTab]} />
      <Container>
        <Row>
          <Col sm="12" lg="4" xl="3" className="project-list">
            <Card>
              <Row>
                <Col>
                  <Nav tabs className="border-tab">
                    <NavItem>
                      <NavLink
                        className={activeTab === "deposit" ? "active" : ""}
                        onClick={() => setTab("deposit")}
                      >
                        <i className="fa fa-bank"></i>
                        Deposit
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "withdrawal" ? "active" : ""}
                        onClick={() => setTab("withdrawal")}
                      >
                        <i className="fa fa-money"></i>
                        Withdrawal
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "transfer" ? "active" : ""}
                        onClick={() => setTab("transfer")}
                      >
                        <i className="fa fa-exchange"></i>
                        Transfer
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "history" ? "active" : ""}
                        onClick={() => setTab("history")}
                      >
                        <i className="fa fa-history"></i>
                        History
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={activeTab === "bonus" ? "active" : ""}
                        onClick={() => setTab("bonus")}
                      >
                        <i className="fa fa-percent"></i>
                        Bonus
                      </NavLink>
                    </NavItem>
                  </Nav>
                </Col>
              </Row>
            </Card>
          </Col>

          <Col sm="12" lg="8" xl="9">
            <TabContent activeTab={activeTab}>
              <TabPane tabId="deposit">
                <Deposit refresh={refresh} />
              </TabPane>
              <TabPane tabId="withdrawal">
                <Withdrawal refresh={refresh} />
              </TabPane>
              <TabPane tabId="transfer">
                <Transfer />
              </TabPane>
              <TabPane tabId="history">
                <History refresh={refresh} />
              </TabPane>
              <TabPane tabId="bonus">
                <Bonus />
              </TabPane>
            </TabContent>
          </Col>
        </Row>
      </Container>
    </>
  )
}

export default Financial
