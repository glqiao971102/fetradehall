import React, { useEffect, useState } from "react"
import { useSelector } from "react-redux"
import { Card, CardBody } from "reactstrap"

import WalletOption from "../options"
import WalletWithdrawal from "./wallet"
import MT5Withdrawal from "./mt5"

const Withdrawal = ({ refresh }) => {
  const [option, setOption] = useState()
  const verification = useSelector((state) => state.user.verification)

  // reset options on tab change
  useEffect(() => {
    setOption(null)
  }, [refresh])

  return (
    <>
      {verification == null ? (
        <>
          <h5 className="mb-3">Withdrawal</h5>
          <Card>
            <CardBody>
              <p style={{ color: "red", fontWeight: "bold" }}>
                Your profile isn’t verified. Please verify your profile in order to withdraw funds.
              </p>
            </CardBody>
          </Card>
        </>
      ) : option == null ? (
        <WalletOption setOption={setOption} type="withdraw" />
      ) : (
        <>
          {option === 1 ? (
            <WalletWithdrawal setOption={setOption} />
          ) : (
            <MT5Withdrawal setOption={setOption} />
          )}
        </>
      )}
    </>
  )
}

export default Withdrawal
