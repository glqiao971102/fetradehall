import React, { useEffect } from "react"
import { Row, Col, Spinner, CardBody, Card } from "reactstrap"
import { useSelector, useDispatch } from "react-redux"

import { HoverCard, ResponsiveCard, ResponsiveIcon, ResponsiveDiv } from "../../components.style"

const SelectAccount = () => {
  const dispatch = useDispatch()
  const accounts = useSelector((state) => state.account.accounts)
  const withdrawal = useSelector((state) => state.withdrawal)

  useEffect(() => {
    if (accounts?.length > 0 && withdrawal?.mt5?.account == null) {
      dispatch({ type: "SET_WITHDRAWAL_MT5_ACCOUNT", account: accounts[0].id })
    }
    // eslint-disable-next-line
  }, [accounts])

  return (
    <>
      <h5>Select MT5 Account</h5>

      <Row>
        {accounts != null ? accounts?.length > 0 &&
          accounts.map((account) => {
            return (
              <Col sm="12" lg="6" key={account.id}>
                <HoverCard
                  isSelected={withdrawal?.mt5?.account === account.id ? "#f9b600" : null}
                  onClick={() => {
                    dispatch({ type: "SET_WITHDRAWAL_MT5_ACCOUNT", account: account.id })
                  }}
                >
                  <ResponsiveCard>
                    <ResponsiveIcon className="icofont icofont-chart-line-alt" />
                    <ResponsiveDiv>
                      <h6>{`${account.account_login}`}</h6>
                      <p>{`Available Balance in account: ${account.balance}`}</p>
                    </ResponsiveDiv>
                  </ResponsiveCard>
                </HoverCard>
              </Col>
            )
          }) : <Col>
            <Card>
              <CardBody style={{ textAlign: "center" }}>
                <Spinner />
              </CardBody>
            </Card>
          </Col>}
      </Row>
    </>
  )
}

export default SelectAccount

// ${account.currency}
