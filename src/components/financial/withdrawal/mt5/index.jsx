import React, { useState, useEffect } from 'react'
import { Row, Form, Col, Card, CardBody, Button } from 'reactstrap'
import { useForm, FormProvider } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import StepZilla from 'react-stepzilla'
import { toast } from 'react-toastify'
import SweetAlert from 'sweetalert2'

import ChooseAmount from './amount'
import SelectCurrency from './currency'
import SelectAccount from './account'
import WalletService from '../../../../network/services/wallet'
import AccountService from '../../../../network/services/account'

const MT5Withdrawal = ({ setOption }) => {
  const dispatch = useDispatch()
  const withdrawal = useSelector((state) => state.withdrawal)
  const methods = useForm()
  const [stage, setStage] = useState(0)

  // use redux for payment_id & wallet_id
  // because form doesn't work with stepzilla
  const handleWithdraw = async (data) => {
    SweetAlert.fire({
      title: 'Are you sure you want to withdraw?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirm',
      cancelButtonText: 'Cancel',
      reverseButtons: true
    }).then(async (result) => {
      if (result.value) {
        try {
          data['account_id'] = withdrawal.mt5.account
          data['wallet_id'] = withdrawal.mt5.wallet
          console.log(data)

          if (data !== '') {
            let result = await AccountService.withdraw(withdrawal.mt5.account, data)
            console.log(result)

            // success
            if (result?.transaction?.id) {
              toast.success('Withdraw Success', {
                position: toast.POSITION.TOP_RIGHT
              })

              methods.reset()
              window.location.reload()
            }
          } else {
            methods.errors.showMessages()
          }
        } catch (error) {
          toast.error(error.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      }
    })
  }

  const init = async () => {
    const walletResponse = await WalletService.get()
    console.log(walletResponse)
    dispatch({ type: 'SET_WALLETS', wallets: walletResponse.wallets })

    const accountResponse = await AccountService.getAll()
    console.log(accountResponse)
    dispatch({ type: 'SET_ACCOUNTS', accounts: accountResponse.accounts })
  }

  useEffect(() => {
    init()
    // eslint-disable-next-line
  }, [])

  return (
    <>
      <Card>
        <CardBody>
          <Row>
            <Col
              sm={12}
              md={4}
              className={`u-pearl ${stage === 0 && 'current'} ${stage > 0 && 'done'}`}
            >
              <span className="u-pearl-number">1</span>
              <span className="u-pearl-title">Select Your Account</span>
            </Col>
            <Col
              sm={12}
              md={4}
              className={`u-pearl ${stage === 1 && 'current'} ${stage > 1 && 'done'}`}
            >
              <span className="u-pearl-number">2</span>
              <span className="u-pearl-title">Choose Wallet Currency</span>
            </Col>
            <Col sm={12} md={4} className={`u-pearl ${stage === 2 && 'current'}`}>
              <span className="u-pearl-number">3</span>
              <span className="u-pearl-title">Choose Amount</span>
            </Col>
          </Row>
        </CardBody>
      </Card>

      <FormProvider {...methods}>
        <Form className="theme-form" onSubmit={methods.handleSubmit(handleWithdraw)}>
          <StepZilla
            steps={[
              {
                name: 'Step 1',
                component: <SelectAccount />
              },
              {
                name: 'Step 2',
                component: <SelectCurrency />
              },
              {
                name: 'Step 2',
                component: <ChooseAmount />
              }
            ]}
            showSteps={false}
            onStepChange={(index) => {
              setStage(index)
            }}
          />
        </Form>
      </FormProvider>

      {stage === 0 && (
        <Button
          color="primary"
          onClick={() => {
            setOption(null)
          }}
        >
          Back
        </Button>
      )}

      {/* Add gap */}
      <div style={{ padding: 24, clear: 'both' }} />
    </>
  )
}

export default MT5Withdrawal
