import React from "react"
import { useSelector } from "react-redux"
import AwepayAmount from "./awepay"
import BluepayAmount from "./bluepay"

const ChooseAmount = () => {
  const method = useSelector((state) => state.withdrawal.wallet.method)

  if (method === 1) {
    return <AwepayAmount />
  } else if (method === 2) {
    return <BluepayAmount />
  }

  return <></>
}

export default ChooseAmount
