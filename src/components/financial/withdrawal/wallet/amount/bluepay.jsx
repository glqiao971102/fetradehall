import React from "react"
import { Row, Col, Card, CardHeader, CardBody, FormGroup, Label, Input, Button } from "reactstrap"
import { useFormContext } from "react-hook-form"

const BluepayAmount = () => {
  const { register, errors, formState } = useFormContext()
  const { isSubmitting } = formState

  return (
    <>
      <Card>
        <CardHeader>
          <h5>Choose Amount</h5>
        </CardHeader>
        <CardBody>
          <Row>
            <Col sm={6}>
              <FormGroup>
                <Label>Withdrawal Amount</Label>
                <Input
                  className="form-control"
                  type="number"
                  name="amount"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>{errors.amount && "Amount is required"}</span>
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <Label>Wallet Address</Label>
                <Input
                  className="form-control"
                  type="text"
                  name="recipient_address"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>{errors.amount && "Address is required"}</span>
              </FormGroup>
            </Col>
          </Row>
          <Button color="primary btn-block" type="submit" style={{ maxWidth: 150, float: "right" }}>
            {isSubmitting ? "Loading..." : "Submit"}
          </Button>
        </CardBody>
      </Card>
    </>
  )
}

export default BluepayAmount
