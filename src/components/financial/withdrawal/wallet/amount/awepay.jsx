import React from "react"
import { Row, Col, Card, CardHeader, CardBody, FormGroup, Label, Input, Button } from "reactstrap"
import { useFormContext } from "react-hook-form"

const AwepayAmount = () => {
  const { register, errors, formState } = useFormContext()
  const { isSubmitting } = formState

  return (
    <>
      <Card>
        <CardHeader>
          <h5>Choose Amount</h5>
        </CardHeader>
        <CardBody>
          <Row>
            <Col sm={6}>
              <FormGroup>
                <Label>Withdrawal Amount</Label>
                <Input
                  className="form-control"
                  type="number"
                  name="amount"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>{errors.amount && "Amount is required"}</span>
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <Label>Account Holder Name</Label>
                <Input
                  className="form-control"
                  type="text"
                  name="account_holder"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>{errors.account_holder && "Name is required"}</span>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col sm={6}>
              <FormGroup>
                <Label>Bank Name</Label>
                <Input
                  className="form-control"
                  type="text"
                  name="bank_name"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>{errors.bank_name && "Bank name is required"}</span>
              </FormGroup>
            </Col>
            <Col sm={6}>
              <FormGroup>
                <Label>Bank Account</Label>
                <Input
                  className="form-control"
                  type="text"
                  name="account_number"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>
                  {errors.account_number && "Bank account is required"}
                </span>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col sm={6}>
              <FormGroup>
                <Label>Bank SWIFT code (BIC)</Label>
                <Input
                  className="form-control"
                  type="text"
                  name="swift_code"
                  innerRef={register({ required: true })}
                />
                <span style={{ color: "red" }}>
                  {errors.swift_code && "Bank SWIFT code (BIC) is required"}
                </span>
              </FormGroup>
            </Col>
          </Row>
          <Button color="primary btn-block" type="submit" style={{ maxWidth: 150, float: "right" }}>
            {isSubmitting ? "Loading..." : "Submit"}
          </Button>
        </CardBody>
      </Card>
    </>
  )
}

export default AwepayAmount
