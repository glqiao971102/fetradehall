import React, { useEffect } from "react"
import { Row, Col } from "reactstrap"
import { useSelector, useDispatch } from "react-redux"

import { HoverCard, ResponsiveCard, ResponsiveIcon, ResponsiveDiv } from "../../components.style"

const SelectCurrency = () => {
  const dispatch = useDispatch()
  const wallets = useSelector((state) => state.wallet.wallets)
  const withdrawal = useSelector((state) => state.withdrawal)

  useEffect(() => {
    if (wallets?.length > 0 && withdrawal.wallet.id == null) {
      dispatch({ type: "SET_WITHDRAWAL_WALLET_ID", id: wallets[0].id })
    }
    // eslint-disable-next-line
  }, [wallets])

  return (
    <>
      <h5>Select Currency</h5>

      <Row>
        {wallets?.length > 0 &&
          wallets.map((wallet) => {
            return (
              <Col sm="12" lg="6" key={wallet.id}>
                <HoverCard
                  isSelected={withdrawal?.wallet?.id === wallet.id ? "#f9b600" : null}
                  onClick={() => {
                    dispatch({ type: "SET_WITHDRAWAL_WALLET_ID", id: wallet.id })
                  }}
                >
                  <ResponsiveCard>
                    <ResponsiveIcon className="icofont icofont-cur-dollar" />
                    <ResponsiveDiv>
                      <h6>{`${wallet.currency?.name}`}</h6>
                      <p>{`Available Balance in wallet: ${wallet.balance}`}</p>
                    </ResponsiveDiv>
                  </ResponsiveCard>
                </HoverCard>
              </Col>
            )
          })}
      </Row>
    </>
  )
}

export default SelectCurrency
