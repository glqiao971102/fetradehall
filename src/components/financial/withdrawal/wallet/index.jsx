import React, { useState, useEffect } from 'react'
import { Row, Form, Col, Card, CardBody, Button } from 'reactstrap'
import { useForm, FormProvider } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import StepZilla from 'react-stepzilla'
import { toast } from 'react-toastify'
import SweetAlert from 'sweetalert2'

import SelectCurrency from './currency'
import SelectPaymentMethod from './payment'
import ChooseAmount from './amount'
import WalletService from '../../../../network/services/wallet'
import PaymentService from '../../../../network/services/payment'

const WalletWithdrawal = ({ setOption }) => {
  const dispatch = useDispatch()
  const withdrawal = useSelector((state) => state.withdrawal)
  const [stage, setStage] = useState(0)
  const methods = useForm()

  // use redux for payment_id & wallet_id
  // because form doesn't work with stepzilla
  const handleWithdraw = async (data) => {
    SweetAlert.fire({
      title: 'Are you sure you want to withdraw?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Confirm',
      cancelButtonText: 'Cancel',
      reverseButtons: true
    }).then(async (result) => {
      if (result.value) {
        try {
          data['wallet_id'] = withdrawal.wallet.id
          data['payment_id'] = withdrawal.wallet.method
          console.log(data)

          if (data !== '') {
            let result = await WalletService.withdraw(data)
            console.log(result)

            // awepay
            if (withdrawal.wallet.method === 1) {
              if (result?.withdraw?.id) {
                // success
                toast.success('Withdraw Success', {
                  position: toast.POSITION.TOP_RIGHT
                })

                methods.reset()
                window.location.reload()
              }
            }

            // bluepay
            if (withdrawal.wallet.method === 2) {
              if (result?.withdraw?.id) {
                // success
                toast.success('Withdraw Success', {
                  position: toast.POSITION.TOP_RIGHT
                })

                methods.reset()
              }
            }
          } else {
            methods.errors.showMessages()
          }
        } catch (error) {
          toast.error(error.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      }
    })
  }

  const init = async () => {
    const walletResponse = await WalletService.get()
    console.log(walletResponse)
    dispatch({ type: 'SET_WALLETS', wallets: walletResponse.wallets })

    const paymentResponse = await PaymentService.getAll()
    console.log(paymentResponse)
    dispatch({ type: 'SET_PAYMENTS', payments: paymentResponse.payments })
  }

  useEffect(() => {
    init()
    // eslint-disable-next-line
  }, [])

  return (
    <>
      <Card>
        <CardBody>
          <Row>
            <Col
              sm={12}
              md={4}
              className={`u-pearl ${stage === 0 && 'current'} ${stage > 0 && 'done'}`}
            >
              <span className="u-pearl-number">1</span>
              <span className="u-pearl-title">Select Currency</span>
            </Col>
            <Col
              sm={12}
              md={4}
              className={`u-pearl ${stage === 1 && 'current'} ${stage > 1 && 'done'}`}
            >
              <span className="u-pearl-number">2</span>
              <span className="u-pearl-title">Select Payment Method</span>
            </Col>
            <Col sm={12} md={4} className={`u-pearl ${stage === 2 && 'current'}`}>
              <span className="u-pearl-number">3</span>
              <span className="u-pearl-title">Choose Amount</span>
            </Col>
          </Row>
        </CardBody>
      </Card>

      <FormProvider {...methods}>
        <Form className="theme-form" onSubmit={methods.handleSubmit(handleWithdraw)}>
          <StepZilla
            steps={[
              {
                name: 'Step 1',
                component: <SelectCurrency />
              },
              {
                name: 'Step 2',
                component: <SelectPaymentMethod />
              },
              {
                name: 'Step 3',
                component: <ChooseAmount />
              }
            ]}
            showSteps={false}
            onStepChange={(index) => {
              setStage(index)
            }}
          />
        </Form>
      </FormProvider>

      {stage === 0 && (
        <Button
          color="primary"
          onClick={() => {
            setOption(null)
          }}
        >
          Back
        </Button>
      )}

      {/* Add gap */}
      <div style={{ padding: 24, clear: 'both' }} />
    </>
  )
}

export default WalletWithdrawal
