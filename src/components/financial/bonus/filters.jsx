import React, { useState, useEffect, Fragment } from "react"
import { Input } from "reactstrap"

const BonusFilters = ({ handleFilter }) => {
  const [fromDate, setFromDate] = useState(null)
  const [toDate, setToDate] = useState(null)

  useEffect(() => {
    console.log(fromDate, toDate)
    let filterObject = {}

    if (fromDate != null) {
      filterObject["from_date"] = fromDate
    }
    if (toDate != null) {
      filterObject["to_date"] = toDate
    }

    handleFilter(filterObject)
    // eslint-disable-next-line
  }, [fromDate, toDate])

  return (
    <Fragment>
      <div className="product-filter">
        <h6 className="f-w-600">From</h6>
        <Input
          className="form-control digits"
          type="date"
          defaultValue={fromDate}
          onChange={(e) => {
            setFromDate(e.currentTarget.value)
          }}
        />
      </div>

      <div className="product-filter">
        <h6 className="f-w-600">To</h6>
        <Input
          className="form-control digits"
          type="date"
          defaultValue={toDate}
          onChange={(e) => {
            setToDate(e.currentTarget.value)
          }}
        />
      </div>
    </Fragment>
  )
}

export default BonusFilters
