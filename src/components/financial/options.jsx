import React from "react"
import { Row, Col } from "reactstrap"

import { HoverCard, ResponsiveCard, ResponsiveIcon, ResponsiveDiv } from "./components.style"

const WalletOption = ({ setOption, type }) => {
  return (
    <>
      <h5 className="mb-3">
        {type === "deposit" ? "Deposit to" : type === "withdraw" ? "Withdraw from" : type === "transfer" ? "Transfer to" : "History of"}
      </h5>
      <Row>
        <Col sm="12" md="6">
          <HoverCard>
            <ResponsiveCard onClick={() => setOption(1)}>
              <ResponsiveIcon className="icofont icofont-wallet" />
              <ResponsiveDiv>
                <h6>Wallet</h6>
                <p>
                  {type === "deposit"
                    ? "via cryptocurrency or bank transfer"
                    : type === "withdraw"
                      ? "to bank or cryptocurrency"
                      : type === "transfer" ? "of other users"
                        : "transactions"}
                </p>
              </ResponsiveDiv>
            </ResponsiveCard>
          </HoverCard>
        </Col>
        <Col sm="12" md="6">
          <HoverCard>
            <ResponsiveCard onClick={() => setOption(2)}>
              <ResponsiveIcon className="icofont icofont-chart-line-alt" />
              <ResponsiveDiv>
                <h6>MT5 Account</h6>
                <p>
                  {type === "deposit"
                    ? "to start your investment"
                    : type === "withdraw"
                      ? "to your wallet"
                      : type === "transfer" ? "between your own MT5 accounts"
                        : "transactions"}
                </p>
              </ResponsiveDiv>
            </ResponsiveCard>
          </HoverCard>
        </Col>
      </Row>
    </>
  )
}

export default WalletOption
