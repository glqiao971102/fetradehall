import React, { useState, useEffect } from "react"
import { Container, Row, Col, Card, Media, CardBody } from "reactstrap"
import styled from "styled-components"

import Breadcrumb from "../../layout/breadcrumb"
import PlatformService from "../../network/services/platform"

const HoverCard = styled(Card)`
  background: ${(props) => props?.background ?? "rgb(15, 60, 90)"};
  cursor: pointer;
  &:hover {
    transform: scale(1.1);
    transition: 0.2s;
  }
`

const Link = styled.a`
  color: white;

  &:hover {
    color: white;
  }
`

const Platform = () => {
  const [platform, setPlatform] = useState({})
  const init = async () => {
    const result = await PlatformService.getAll()
    console.log(result)

    setPlatform(result.platform)
  }

  useEffect(() => {
    init()
  }, [])

  return (
    <>
      <Breadcrumb title={"Platforms"} />
      <Container>
        <Card>
          <CardBody>
            <h5>Get your MetaTrader 5</h5>
            <p>One of the most powerful trading platforms for Windows, OS X and mobile devices</p>
          </CardBody>
        </Card>
        <Row>
          <Col sm="6" lg="4">
            <HoverCard background="#F25022">
              <Link href={platform?.windows ?? "#"}>
                <CardBody>
                  <Media>
                    <i className="icofont icofont-brand-windows" style={{ fontSize: 40 }}></i>
                    <Media body>
                      <h6
                        className="pull-right"
                        style={{ padding: 0, margin: 0, fontSize: 30, color: "white" }}
                      >
                        Windows
                      </h6>
                    </Media>
                  </Media>
                </CardBody>
              </Link>
            </HoverCard>
          </Col>
          <Col sm="6" lg="4">
            <HoverCard background="#78C257">
              <Link href={platform?.android ?? "#"}>
                <CardBody>
                  <Media>
                    <i className="icofont icofont-brand-android-robot" style={{ fontSize: 40 }}></i>
                    <Media body>
                      <h6
                        className="pull-right"
                        style={{ padding: 0, margin: 0, fontSize: 30, color: "white" }}
                      >
                        Android
                      </h6>
                    </Media>
                  </Media>
                </CardBody>
              </Link>
            </HoverCard>
          </Col>
          <Col sm="6" lg="4">
            <HoverCard background="black" >
              <Link href={platform?.ios ?? "#"}>
                <CardBody>
                  <Media>
                    <i className="icofont icofont-brand-apple" style={{ fontSize: 40, color: "#F9F6EF" }}></i>
                    <Media body>
                      <h6
                        className="pull-right"
                        style={{ padding: 0, margin: 0, fontSize: 30, color: "#F9F6EF" }}
                      >
                        iOS
                      </h6>
                    </Media>
                  </Media>
                </CardBody>
              </Link>
            </HoverCard>
          </Col>
          <Col sm="6" lg="4">
            <HoverCard background="#4285F4">
              <Link href={platform?.web ?? "#"}>
                <CardBody>
                  <Media>
                    <i className="icofont icofont-monitor" style={{ fontSize: 40 }}></i>
                    <Media body>
                      <h6
                        className="pull-right"
                        style={{ padding: 0, margin: 0, fontSize: 30, color: "white" }}
                      >
                        Web
                      </h6>
                    </Media>
                  </Media>
                </CardBody>
              </Link>
            </HoverCard>
          </Col>
        </Row>
      </Container>
    </>
  )
}

export default Platform
