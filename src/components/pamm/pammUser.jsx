import React from 'react'
import { Row, Card, CardTitle, Col,CardText, Button, CardBody } from "reactstrap";
import ChartData from './chart'

const pammUser = props => {
    return (
        <Row>
        {props.loadedUser.map(user => 
            (
              <Col key={user.id}  md="6" lg="4">
                <Card body>

                  <CardBody style={{padding: 0}}> <ChartData/> </CardBody>

                  <CardBody style={{padding: 0, marginTop: "20px"}}>
                    <CardTitle className="float-left" tag="h5">{user.name}</CardTitle>
                    <Button className="float-right" color="warning">Follow for {user.price}USD</Button>
                  </CardBody>
    
                  <CardBody style={{padding: 0}}>
                    <CardBody className="float-left">
                      <CardTitle tag="h4">Growth</CardTitle>
                      <CardText>{user.growth}%</CardText>
                    </CardBody>  
    
                    <CardBody className="float-right" >
                      <CardTitle tag="h4">Follower</CardTitle>
                      <CardText>{user.followers}</CardText>
                    </CardBody>
                  </CardBody>
        
                </Card>
            </Col>
            ))}
        </Row>
    )
}

export default pammUser