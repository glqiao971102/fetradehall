import React, { useState, useEffect } from 'react'
import { Line } from 'react-chartjs-2'

const Chart = (props) =>{

  const [dataChart, setDataChart] = useState({})

  const chart = () => {
    setDataChart({
      labels: ['Jan', 'Feb', "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      datasets: [
        {
          label: "Growth 2019",
          data: [29, 45, 38, 10, 5, 34, 15, 67, 56, 43,67,14],
          backgroundColor: [
            "white"
          ],
          borderWidth: 1,
          borderColor: "#16415e",
          pointRadius: 0
          
        }
      ]
    })
  }

  useEffect( () => {
    chart()
  }, [])

  return(
    <>
    
    <Line data={dataChart} options={{ 
      legend: {display:false},
      responsive: true,
      scales: {
        yAxes: [
          {
            ticks: {
              display: false
            },
            gridLines:{
              display: false
            }
          }
        ],
        xAxes:[
          {
            gridLines:{
              display: false
            }
          }
        ],
      }
    }} />
    </>
  )
  
}

export default Chart