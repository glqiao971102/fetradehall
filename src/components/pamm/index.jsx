import React, { useEffect ,useState} from "react";
import axios from 'axios'

import Breadcrumb from "../../layout/breadcrumb";
import PammUser from './pammUser'
import { Button, CardBody } from "reactstrap";
import PammTable from './table'

const UserPamm = (props) => {

  const [userInformation, setUserInformation] = useState([])
  const loadedUserInformation = []
  const [showGraph, setShowGraph] = useState(true)

  useEffect(()=>{
    console.log('This is from Pamm User')
    axios.get("https://pammuser-13a30.firebaseio.com/userInformation.json")
      .then(response => {
        for(let key in response.data){
          loadedUserInformation.push({
            id: Math.floor(Math.random() * 1000),
            name: response.data[key].username,
            growth: response.data[key].growth,
            followers: response.data[key].followers,
            funds: response.data[key].funds,
            growthAnalysis: response.data[key].growthAnalysis,
            win: response.data[key].win,
            price: response.data[key].price
          })
        }
        setUserInformation(loadedUserInformation)
      })
      .catch(error => console.log(error))
  }, [])

  return (
    <>
    {console.log(loadedUserInformation)}
      <Breadcrumb parent="Pamm" title="PAMM" />
      <Button onClick={() => setShowGraph(!showGraph)}>{showGraph ? "Show In Table" : "Show Graph"}</Button>
      
      <CardBody>
        <h5>All User</h5>
        { 
          showGraph ? <PammUser loadedUser = {userInformation}/> : <PammTable loadedUsers = {userInformation}/>
        }
      </CardBody>
    
    </>
  );
};

export default UserPamm;
