import React, { useEffect, useState } from "react"
import DataTable from "react-data-table-component"
import { Button } from "reactstrap"

const loadedUsers = []

const columns = [
  {
    name: "User ID",
    selector: "id",
    sortable: true,
  },
  {
    name: "Price (USD)",
    selector: "price",
    sortable: true,
  },
  {
    name: "Funds (USD)",
    selector: "funds",
    sortable: true,
  },
  {
    name: "Growth (%)",
    selector: "growth",
    sortable: true,
  },
  {
    name: "Win (%)",
    selector: "win",
    sortable: true,
  },
  {
    name: 'Poster Button',
    button: true,
    cell: () => <Button>Follow</Button>,
  },
]

const RiskTable = (props) => {

  return (
    <>
    <DataTable noHeader data={props.loadedUsers} columns={columns} striped={true} center={true}/>
    </>

  )
  
  
}

export default RiskTable
