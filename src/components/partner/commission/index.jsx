import React from "react";
import { Card, CardBody } from "reactstrap";
import CommissionTable from "./table";

const Commissions = () => {
  return (
    <Card>
      <CardBody>
        <CommissionTable />
      </CardBody>
    </Card>
  );
};

export default Commissions;
