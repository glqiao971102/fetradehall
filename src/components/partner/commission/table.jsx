import React, { useState, useEffect } from "react";
import DataTable from "react-data-table-component";
import moment from "moment"

import UserService from "../../../network/services/user"

const columns = [
  {
    name: "Level",
    selector: "level",
    sortable: true,
  },
  {
    name: "Login",
    selector: "account",
    sortable: true,
    format: (row) => {
      return row.account?.account_login ?? "-"
    }
  },
  {
    name: "Name",
    selector: "user.name",
    sortable: true,
  },
  {
    name: "Group",
    selector: "account",
    sortable: true,
    format: (row) => {
      return row.account?.account_login ?? "-"
    }
  },
  {
    name: "Symbol",
    selector: "symbol",
    sortable: true,
  },
  {
    name: "Volume",
    selector: "volume",
    sortable: true,
    right: true,
  },
  {
    name: "Commission",
    selector: "amount",
    sortable: true,
    right: true,
  },
  {
    name: "Currency",
    selector: "currency_unit",
    sortable: true,
  },
  {
    name: "Time",
    selector: "updated_at",
    sortable: true,
    width: "200px",
    format: (row) => {
      return moment(row.updatedAt).format("yyyy-MM-DD HH:mmA")
    }
  },
];

const CommissionTable = () => {
  const [isBusy, setIsBusy] = useState(false)
  const [total, setTotal] = useState(0)
  const [data, setData] = useState([])

  const handlePageChange = async (page) => {
    const result = await UserService.getCommissions({ page })
    setData(result.commissions?.data ?? [])
  }
  
  const fetchCommissions = async () => {
    try {
      setIsBusy(true)
      const result = await UserService.getCommissions()
      console.log(result)

      setTotal(result.commissions?.meta?.total ?? 0)
      setData(result.commissions?.data ?? [])
      setIsBusy(false)
    } catch (error) {
      console.log(error)
      setIsBusy(false)
    }
  }

  useEffect(() => {
    fetchCommissions()
  }, [])

  return (
    <DataTable
      noHeader
      data={data}
      columns={columns}
      striped={true}
      center={true}
      pagination
      paginationComponentOptions={{ noRowsPerPage: true }}
      paginationServer
      paginationTotalRows={total}
      onChangePage={handlePageChange}
      style={{ paddingBottom: 8 }}
      progressPending={isBusy}
    />
  );
};

export default CommissionTable;
