import React, { useEffect, useState } from 'react'
import Tree, { TreeNode } from 'rc-tree'
import { Spinner } from 'reactstrap'

import 'rc-tree/assets/index.css'
import './tree.scss'
import ClientService from '../../../network/services/client'

const Icon = ({ selected }) => <span className={`customize-icon ${selected && 'selected-icon'})`} />

const ClientTree = () => {
  const fetchClients = async () => {
    const result = await ClientService.getAll()
    console.log(result)

    setData(result.clients)
  }

  useEffect(() => {
    fetchClients()
  }, [])

  // tree
  const [data, setData] = useState(null)
  //   const onSelect = (selectedKeys, info) => {
  //     console.log("selected", selectedKeys, info)
  //   }

  //   const onCheck = (checkedKeys, info) => {
  //     console.log("onCheck", checkedKeys, info)
  //   }

  useEffect(() => {
    console.log(data)
  }, [data])

  return (
    <>
      {data != null ? (
        <Tree
          icon={Icon}
          showLine
          //   checkable
          //   defaultExpandAll
          //   defaultExpandedKeys={this.state.defaultExpandedKeys}
          //   onExpand={this.onExpand}
          //   defaultSelectedKeys={this.state.defaultSelectedKeys}
          //   defaultCheckedKeys={this.state.defaultCheckedKeys}
          //   onSelect={onSelect}
          //   onCheck={onCheck}
          treeData={data}
          titleRender={(item) => {
            return (
              <>
                <span>{item?.email ?? '-'}</span>
                <span>{` <Commission: ${item?.commision ?? '-'}>`}</span>
                <span>{` <Volume: ${item?.volume ?? '-'}>`}</span>
                {/* <a href="">
                  <span>{item?.volume ?? '-'}</span>
                </a> */}
              </>
            )
          }}
          // onActiveChange={(key) => console.log("Active:", key)}
        />
      ) : (
        <Spinner />
      )}
    </>
  )
}

export default ClientTree
