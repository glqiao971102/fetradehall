import React from "react"
import { Card, CardBody } from "reactstrap"
import ClientTree from "./tree"

const Clients = () => {
  return (
    <Card>
      <CardBody>
        <ClientTree />
      </CardBody>
    </Card>
  )
}

export default Clients
