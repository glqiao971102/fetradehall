import React, { useEffect, useRef, useState } from "react"
import DataTable from "react-data-table-component"
import { Row, Col, Card, CardHeader, Input } from "reactstrap"
import moment from "moment"

import ClientService from "../../../network/services/client"
import ClientFilters from "./filters"

const columns = [
  {
    name: "Clients",
    selector: "name",
    sortable: true,
  },
  {
    name: "Wallet Deposits",
    selector: "deposit",
    sortable: true,
    right: true,
    format: (row) => {
      let currency = ""
      let total = 0

      row.accounts.forEach((cur) => {
        let sum = 0
        cur.account_deposits.forEach((deposit) => {
          sum += deposit.debit_amount
          currency = deposit.currency_unit
        })

        total += sum
      })

      return total + " " + currency
    },
  },
  {
    name: "Wallet Withdrawals",
    selector: "withdraw",
    sortable: true,
    right: true,
    format: (row) => {
      let currency = ""
      let total = 0

      row.accounts.forEach((cur) => {
        let sum = 0
        cur.account_withdraws.forEach((deposit) => {
          sum += deposit.credit_amount
          currency = deposit.currency_unit
        })

        total += sum
      })

      return total + " " + currency
    },
  },
  {
    name: "Volume",
    selector: "volume",
    sortable: true,
    right: true,
  },
  {
    name: "Commission",
    selector: "commission",
    sortable: true,
    right: true,
  },
  {
    name: "Country",
    selector: "country",
    sortable: true,
  },
  {
    name: "Registration Date",
    selector: "created_at",
    sortable: true,
    width: "200px",
    format: (row) => {
      if (row.created_at != null) {
        return moment(row.created_at).format("yyyy-MM-DD")
      }

      return "-"
    },
  },
]

const ClientTable = () => {
  const [isBusy, setIsBusy] = useState(false)
  const [data, setData] = useState([])
  const [total, setTotal] = useState(0)
  const [filters, setFilters] = useState(null)
  const [sidebaron, setSidebaron] = useState(true)
  const [searchKeyword, setSearchKeyword] = useState("")
  const wrapperRef = useRef()

  const fetchClients = async () => {
    try {
      setIsBusy(true)
      const result = await ClientService.getAll()
      console.log(result)

      setTotal(result.clients?.meta?.total ?? 0)
      setData(result.clients?.data ?? [])
      setIsBusy(false)
    } catch (error) {
      console.log(error)
      setIsBusy(false)
    }
  }

  const handleFilter = async () => {
    setIsBusy(true)
    const result = await ClientService.getAll({
      filters: {
        ...filters,
      },
    })
    setData(result.clients.data)
    setIsBusy(false)
  }

  const handlePageChange = async (page) => {
    setIsBusy(true)
    const result = await ClientService.getAll({ page })
    setData(result.clients.data)
    setIsBusy(false)
  }

  const handleSearchKeyword = (keyword) => {
    setSearchKeyword(keyword)
    setFilters({
      client: keyword,
    })
  }

  const onClickFilter = () => {
    if (sidebaron) {
      setSidebaron(false)
      wrapperRef.current.classList.add("sidebaron")
    } else {
      setSidebaron(true)
      wrapperRef.current.classList.remove("sidebaron")
    }
  }

  useEffect(() => {
    fetchClients()
  }, [])

  useEffect(() => {
    handleFilter()
    // eslint-disable-next-line
  }, [filters])

  return (
    <div className="product-wrapper" ref={wrapperRef}>
      <div className="product-grid" style={{ minHeight: "50vh" }}>
        <Row>
          <Col xl="3">
            <div className="product-sidebar">
              <div className="filter-section">
                <Card className="m-0">
                  <CardHeader>
                    <h6 className="mb-0 f-w-600">
                      Filters
                      <span className="pull-right">
                        <i className="fa fa-chevron-down toggle-data" onClick={onClickFilter}></i>
                      </span>
                    </h6>
                  </CardHeader>
                  <div className="left-filter">
                    <div style={{ padding: 16 }} />
                    <ClientFilters handleFilter={setFilters} />
                  </div>
                </Card>
              </div>
            </div>
          </Col>
          <Col xl="9" sm="12">
            <div style={{ height: "100%", display: "flex", alignItems: "center" }}>
              <Input
                className="form-control"
                type="text"
                placeholder="Search Client"
                defaultValue={searchKeyword}
                onChange={(e) => handleSearchKeyword(e.target.value)}
              />
            </div>
          </Col>
        </Row>

        <div className="product-wrapper-grid" style={{ marginTop: 16 }}>
          <DataTable
            noHeader
            data={data}
            columns={columns}
            striped={true}
            center={true}
            pagination
            paginationComponentOptions={{ noRowsPerPage: true }}
            paginationServer
            paginationTotalRows={total}
            onChangePage={handlePageChange}
            progressPending={isBusy}
            style={{ padding: 10 }}
          />
        </div>
      </div>
    </div>
  )
}

export default ClientTable
