import React from "react"
import { Card, Button, CardHeader, CardBody, Spinner, Row, Col } from "reactstrap"
import { useSelector } from "react-redux"
import { CopyToClipboard } from "react-copy-to-clipboard"
import { toast } from "react-toastify"

const Referral = () => {
  const partners = useSelector((state) => state.user.partners)

  return (
    <>
      <Card>
        <CardHeader>
          <h5>Referral Link</h5>
        </CardHeader>
        <CardBody>
          {partners != null ? (
            partners?.length > 0 ? (
              partners.map((partner) => {
                return (
                  <Card key={partner.id}>
                    <CardHeader className="bg-secondary" style={{ padding: 20 }}>
                      <h5 className="text-center">IB-{partner?.ib_code}</h5>
                    </CardHeader>
                    <CardBody style={{ padding: 16, textAlign: "center" }}>
                      <p>
                        https://tradehall.vercel.app/auth?referral_code={partner?.referral_code}
                      </p>
                      <Row style={{ marginTop: 12 }}>
                        <Col className="text-right">
                          <CopyToClipboard
                            text={
                              "https://tradehall.vercel.app/auth?referral_code=" +
                              partner?.referral_code
                            }
                          >
                            <Button
                              color="primary"
                              onClick={() => {
                                toast.success("Link copied to clipboard!", {
                                  position: toast.POSITION.BOTTOM_RIGHT,
                                })
                              }}
                            >
                              Copy Link
                            </Button>
                          </CopyToClipboard>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                )
              })
            ) : (
              <p>No record found</p>
            )
          ) : (
            <div style={{ textAlign: "center" }}>
              <Spinner />
            </div>
          )}
          {/*
          <div style={{ display: "flex" }}>
            <Input className="form-control" name="referral_link" defaultValue={link} />
            <CopyToClipboard text={link}>
              <Button
                color="primary"
                className="notification"
                onClick={() =>
                  toast.success("Link copied to clipboard!", {
                    position: toast.POSITION.BOTTOM_RIGHT,
                  })
                }
                style={{ marginLeft: 12 }}
              >
                Copy
              </Button>
            </CopyToClipboard>
          </div> */}
        </CardBody>
      </Card>
    </>
  )
}

export default Referral
