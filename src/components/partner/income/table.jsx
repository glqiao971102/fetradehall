import React, { useState } from "react"
import DataTable from "react-data-table-component"

const tableData = [
  {
    name: "a",
    account: "12345",
    level: 1,
    ib: 1,
    deposit: 1,
    withdraw: 1,
    lots: 1,
    commission: 1,
  },
]

const columns = [
  {
    name: "Name",
    selector: "name",
    sortable: true,
  },
  {
    name: "Account",
    selector: "account",
    sortable: true,
  },
  {
    name: "Level",
    selector: "level",
    sortable: true,
  },
  {
    name: "IB",
    selector: "ib",
    sortable: true,
    right: true,
  },
  {
    name: "Total Deposit",
    selector: "deposit",
    sortable: true,
    right: true,
  },
  {
    name: "Total Withdraw",
    selector: "withdraw",
    sortable: true,
    right: true,
  },
  {
    name: "Lots",
    selector: "lots",
    sortable: true,
  },
  {
    name: "Commission",
    selector: "commission",
    sortable: true,
  },
]

const IncomeTable = () => {
  const [data, setData] = useState(tableData)

  return <DataTable noHeader data={data} columns={columns} striped={true} center={true} />
}

export default IncomeTable
