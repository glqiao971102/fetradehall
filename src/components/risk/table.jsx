import React, { useState } from "react"
import DataTable from "react-data-table-component"

const tableData = [
  {
    id: 321247,
    category: "Stop Risk",
    account: 512345,
    type: 1,
    symbol: 1,
    price: 1,
    profit: 1,
    volume: 1,
    opened: 1,
    closed: 1,
  },
  {
    id: 321248,
    category: "Stop Out",
    account: 512345,
    type: 1,
    symbol: 1,
    price: 1,
    profit: 1,
    volume: 1,
    opened: 1,
    closed: 1,
  },
]

const columns = [
  {
    name: "Ticket",
    selector: "id",
    sortable: true,
  },
  {
    name: "Category",
    selector: "category",
    sortable: true,
  },
  {
    name: "Accounts",
    selector: "account",
    sortable: true,
  },
  {
    name: "Type",
    selector: "type",
    sortable: true,
  },
  {
    name: "Symbol",
    selector: "symbol",
    sortable: true,
  },
  {
    name: "Price",
    selector: "price",
    sortable: true,
  },
  {
    name: "Profit",
    selector: "profit",
    sortable: true,
  },
  {
    name: "Volume",
    selector: "volume",
    sortable: true,
  },
  {
    name: "Opened",
    selector: "open",
    sortable: true,
  },
  {
    name: "Closed",
    selector: "close",
    sortable: true,
  },
]

const RiskTable = () => {
  const [data, setData] = useState(tableData)

  return <DataTable noHeader data={data} columns={columns} striped={true} center={true} />
}

export default RiskTable
