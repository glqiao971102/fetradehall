import React, { useEffect, useState } from "react"
import { Card, CardBody, Container, Spinner } from "reactstrap"
import { useLocation } from "react-router-dom"

import Breadcrumb from "../../layout/breadcrumb"

import TransactionService from "../../network/services/transaction"

const PaymentPostback = () => {
  const location = useLocation()
  const [success, setSuccess] = useState(null)
  const [transaction, setTransaction] = useState(null)

  const fetchTransaction = async (id) => {
    try {
      const result = await TransactionService.get(id)
      console.log(result)
      setTransaction(result.transaction)
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    const params = new URLSearchParams(location.search)

    const status = params.get("status")
    if (status?.toLowerCase() === "success") {
      setSuccess(true)
    }

    const id = params.get("transaction_id")
    fetchTransaction(id)
    // eslint-disable-next-line
  }, [location])

  return (
    <>
      <Breadcrumb title="Payment" />
      <Container>
        {success != null && transaction != null ? (
          <Card className="ribbon-wrapper">
            <CardBody>
              <div className={`ribbon ribbon-${success ? "success" : "danger"}`}>
                {success ? "Success" : "Failed"}
              </div>
              <p>Ticket: {transaction?.id}</p>
              <p>Amount: {transaction?.debit_amount}</p>
              <p>Currency: {transaction?.currency_unit}</p>
              <p>Method: {transaction?.deposit?.payment_method}</p>
            </CardBody>
          </Card>
        ) : (
          <Card>
            <CardBody style={{ textAlign: "center" }}>
              <Spinner />
            </CardBody>
          </Card>
        )}
      </Container>
    </>
  )
}

export default PaymentPostback
