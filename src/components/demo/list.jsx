import React, { useState, useEffect } from "react"
import { Card, CardBody, CardHeader, Col, Row, Spinner } from "reactstrap"
import { useSelector, useDispatch } from "react-redux"

import PlanService from "../../network/services/plan"

const DemoAccountList = () => {
  const dispatch = useDispatch()
  const account = useSelector((state) => state.demo.account)

  const [accounts, setAccounts] = useState(null)

  const init = async () => {
    // demo accounts
    const result = await PlanService.getAll({
      filters: {
        account_type: 1,
      },
    })
    console.log(result.plan)
    setAccounts(result.plan)

    if (result.plan?.length > 0) {
      dispatch({ type: "SET_DEMO_ACCOUNT", account: result.plan[0] })
    }
  }

  useEffect(() => {
    init()
    // eslint-disable-next-line
  }, [])

  return (
    <div className="text-center">
      <Row>
        {accounts != null ? (
          accounts?.length > 0 &&
          accounts.map((item, index) => {
            return (
              <Col xl="4" md="6" key={item.id}>
                <Card
                  className="card-absolute"
                  key={index}
                  onClick={() => {
                    dispatch({ type: "SET_DEMO_ACCOUNT", account: item })
                  }}
                  style={{
                    borderColor: account?.id === item.id ? "#f9b600" : null,
                  }}
                >
                  <CardHeader className="bg-secondary">
                    <h5>{item.name}</h5>
                  </CardHeader>
                  <CardBody className="p-3">
                    <div>
                      <p>Minimum Deposit {item.min_deposit}</p>
                      <p>{item.min_lot} Minimum Lot</p>
                      <p>Spread from {item.spread} points</p>
                      <p>Commission level {item.commission}</p>
                    </div>
                  </CardBody>
                </Card>
              </Col>
            )
          })
        ) : (
          <Col>
            <Card>
              <CardBody>
                <Spinner />
              </CardBody>
            </Card>
          </Col>
        )}
      </Row>
    </div>
  )
}

export default DemoAccountList
