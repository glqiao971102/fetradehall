import React from "react"
import { LogOut, User } from "react-feather"
import { Link } from "react-router-dom"
import { useSelector } from "react-redux"

import Auth from "../../../network/services/auth"

const Profile = () => {
  const currentUser = useSelector((state) => state.user.user)
  const logOut = () => {
    Auth.logout()
  }

  return (
    <>
      <div className="media profile-media">
        <div className="media-body">
          <span>{currentUser?.name ?? "Name"}</span>
          <p className="mb-0 font-roboto">
            {currentUser?.email ?? "Email"} <i className="middle fa fa-angle-down"></i>
          </p>
        </div>
      </div>
      <ul className="profile-dropdown onhover-show-div">
        <li>
          <Link to={`${process.env.PUBLIC_URL}/account`} href="#javascript">
            <User />
            <span>Account</span>
          </Link>
        </li>
        <li onClick={logOut}>
          <LogOut />
          <span>Log Out</span>
        </li>
      </ul>
    </>
  )
}

export default Profile
