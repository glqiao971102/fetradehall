import React, { useState, useEffect } from 'react'
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Button,
  Spinner,
  Modal,
  ModalHeader,
  ModalBody
} from 'reactstrap'
import { useHistory } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'

import LiveSettings from '../common/settings'
import MasterPassword from '../common/settings/master_password'
import InvestorPassword from '../common/settings/investor_password'
import ChangeLeverage from '../common/settings/leverage'

import { ResponsiveDiv, StyledRow } from '../common/components.style'

import AccountService from '../../network/services/account'

const settings = [
  {
    id: 1,
    title: 'Change Master Password',
    cta: <MasterPassword />
  },
  {
    id: 2,
    title: 'Change Investor Password',
    cta: <InvestorPassword />
  },
  {
    id: 3,
    title: 'Change Leverage',
    cta: <ChangeLeverage />
  }
  // {
  //   id: 4,
  //   title: "Change Stop Risk",
  //   cta: <ChangeStopRisk />,
  // },
]

const ActiveLiveAccounts = () => {
  let history = useHistory()
  const dispatch = useDispatch()
  const currencies = useSelector((state) => state.currency.currencies)

  const navigate = (path) => {
    history.push(path)
    dispatch({ type: 'SET_ACTIVE_MENU', path: path })
  }

  // live accounts
  const liveAccounts = useSelector((state) => {
    if (state.account.accounts?.length > 0) {
      return state.account.accounts.filter((e) => e.account_type === 0)
    }

    return []
  })
  const selectedAccount = useSelector((state) => state.dashboard.account)
  const init = async () => {
    const accountResponse = await AccountService.getAll()
    dispatch({ type: 'SET_ACCOUNTS', accounts: accountResponse.accounts })
  }

  useEffect(() => {
    init()
    // eslint-disable-next-line
  }, [])

  // modal
  const [showModal, setShowModal] = useState(false)
  const [selectedSetting, setSelectedSetting] = useState(null)
  const toggleModal = () => {
    setShowModal(!showModal)
  }

  useEffect(() => {
    if (selectedSetting != null && selectedAccount != null) {
      setShowModal(true)
    }
  }, [selectedSetting, selectedAccount])

  useEffect(() => {
    if (showModal === false) {
      setSelectedSetting(null)
      dispatch({
        type: 'SET_DASHBOARD_ACCOUNT',
        account: null
      })
    }
    // eslint-disable-next-line
  }, [showModal])

  return (
    <>
      {liveAccounts != null ? (
        liveAccounts?.length > 0 ? (
          liveAccounts.map((account) => {
            const currency = currencies.find((e) => {
              return e.id === account.currency_id
            })

            return (
              <Card className="card-absolute" key={account.id}>
                <CardHeader className="bg-primary">
                  <h6 style={{ margin: 0 }}>
                    {`#${account.account_login} ${account.plan?.name} - LIVE`}
                  </h6>
                </CardHeader>
                <div className="card-right">
                  <ResponsiveDiv>
                    <Button
                      onClick={() => {
                        navigate(`${process.env.PUBLIC_URL}/financial`)
                      }}
                    >
                      Deposit
                    </Button>

                    <LiveSettings
                      settings={settings}
                      setSelectedSetting={setSelectedSetting}
                      account={account}
                    />
                  </ResponsiveDiv>
                </div>
                <CardBody className="text-center">
                  <StyledRow>
                    <Col sm={6} lg={4}>
                      <h6>{account.plan?.name ?? '-'}</h6>
                      <p>ACCOUNT TYPE</p>
                    </Col>
                    <Col sm={6} lg={4}>
                      <h6>1:{account?.leverage ?? "-"}</h6>
                      <p>LEVERAGE</p>
                    </Col>
                    <Col sm={6} lg={4}>
                      <h6>{`${account?.balance ?? "-"} ${currency?.name ?? ''}`}</h6>
                      <p>BALANCE</p>
                    </Col>
                    <Col sm={6} lg={4}>
                      <h6>{`${account?.credit ?? "-"} ${currency?.name ?? ''}`}</h6>
                      <p>CREDIT</p>
                    </Col>
                    <Col sm={6} lg={4}>
                      <h6>{`${account?.equity ?? '-'} ${currency?.name ?? ''}`}</h6>
                      <p>EQUITY</p>
                    </Col>
                    <Col sm={6} lg={4}>
                      <h6>{`${account?.free_margin ?? "-"} ${currency?.name ?? ''}`}</h6>
                      <p>FREE MARGIN</p>
                    </Col>
                    {/* <Col sm={6} lg={4}>
                      <h6>{`${account.stop_risk ?? '-'} ${currency?.name ?? ''}`}</h6>
                      <p>STOP RISK</p>
                    </Col> */}
                    {/* <Col sm={6} lg={4}>
                      <h6>-</h6>
                      <p>AGENT CODE</p>
                    </Col> */}
                  </StyledRow>
                </CardBody>
              </Card>
            )
          })
        ) : (
          <Card>
            <CardBody className="text-center">No active accounts</CardBody>
          </Card>
        )
      ) : (
        <div style={{ textAlign: 'center' }}>
          <Spinner />
        </div>
      )}

      {selectedSetting !== null && selectedAccount !== null && (
        <Modal isOpen={showModal} backdrop={true} centered={true}>
          <ModalHeader toggle={toggleModal}>{settings[selectedSetting].title}</ModalHeader>
          <ModalBody>{settings[selectedSetting].cta}</ModalBody>
        </Modal>
      )}
    </>
  )
}

export default ActiveLiveAccounts
