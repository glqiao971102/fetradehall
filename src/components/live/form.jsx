import React, { useState, useEffect } from "react"
import {
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  Card,
  CardBody,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap"
import { useHistory } from "react-router-dom"
import { useForm } from "react-hook-form"
import { useSelector, useDispatch } from "react-redux"
import styled from "styled-components"

import AccountService from "../../network/services/account"

const List = styled.li`
  width: 33.33%;
  text-transform: capitalize;

  @media (max-width: 768px) {
    width: 100%;
  }
`

const LiveForm = () => {
  let history = useHistory()
  const dispatch = useDispatch()
  const [currency, setCurrency] = useState(null)
  const [result, setResult] = useState(null)
  const [error, setError] = useState(null)
  const account = useSelector((state) => state.live.account)
  const currencies = useSelector((state) => state.currency.currencies)
  const { register, handleSubmit, errors, watch, formState } = useForm()
  const { isSubmitting } = formState
  const watchIb = watch("ib_select", "none")

  // modal
  const [showModal, setShowModal] = useState(false)
  const toggle = () => {
    setShowModal(!showModal)
  }

  useEffect(() => {
    if (showModal !== true && result !== null) {
      window.location.reload()
    }
  // eslint-disable-next-line
  }, [showModal])

  const handleCreateAccount = async (data) => {
    console.log(data)

    try {
      data["plan_id"] = account?.id
      data["currency_id"] = currency?.id

      if (data !== "") {
        const result = await AccountService.createLive(data)
        console.log(result)

        // success
        if (result?.account?.id) {
          setResult(result.account)
        }
      } else {
        errors.showMessages()
      }
    } catch (error) {
      setError(error.message)
    }
  }

  const navigate = (path) => {
    history.push(path)
    dispatch({ type: "SET_ACTIVE_MENU", path: path })
  }

  useEffect(() => {
    if (result != null) {
      toggle()
    }
    // eslint-disable-next-line
  }, [result])

  useEffect(() => {
    let selectedCurrency = currencies.find((e) => e.id === account.currency_id)
    setCurrency(selectedCurrency?.name ?? "")
    // eslint-disable-next-line
  }, [])

  return (
    <>
      <Card>
        <CardBody>
          <Form className="theme-form" onSubmit={handleSubmit(handleCreateAccount)}>
            <Row>
              <Col>
                <FormGroup>
                  <Label>Account Type</Label>
                  <Input
                    className="form-control"
                    type="text"
                    name="type"
                    defaultValue={account?.name ?? ""}
                    readOnly
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup>
                  <Label>Currency</Label>
                  <Input
                    className="form-control"
                    type="text"
                    name="currency"
                    defaultValue={currency ?? ""}
                    readOnly
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup>
                  <Label>Leverage</Label>
                  <Input
                    type="select"
                    name="leverage"
                    className="form-control digits"
                    innerRef={register({ required: true })}
                  >
                    <option value="50">50</option>
                    <option value="100">100</option>
                    <option value="200">200</option>
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup>
                  <Label>Introducing Broker</Label>
                  <Input
                    type="select"
                    name="ib_select"
                    className="form-control digits"
                    innerRef={register({ required: true })}
                  >
                    <option value="none">No Introducing Broker</option>
                    <option value="exist">Existing Introducing Broker</option>
                  </Input>
                </FormGroup>
              </Col>
              {watchIb !== "none" && (
                <Col>
                  <FormGroup>
                    <Label>IB Code</Label>
                    <Input
                      className="form-control"
                      type="text"
                      name="ib_code"
                      innerRef={register({ required: true })}
                    />
                  </FormGroup>
                </Col>
              )}
            </Row>
            <Row>
              <Col>
                <FormGroup>
                  <Label>Stop Risk Percentage</Label>
                  <Input
                    type="select"
                    name="stop_risk"
                    className="form-control digits"
                    innerRef={register({ required: true })}
                  >
                    <option value="100">100%</option>
                    <option value="50">50%</option>
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            {error != null && <p style={{ color: "red" }}>{error}</p>}
            <Row>
              <Col>
                <FormGroup className="mb-0" style={{ float: "right" }}>
                  <Button color="success" disabled={isSubmitting}>
                    {isSubmitting ? "Loading..." : "Submit"}
                  </Button>
                </FormGroup>
              </Col>
            </Row>
          </Form>
        </CardBody>
      </Card>
      <Card>
        <CardBody>
          <h5>Trading Conditions</h5>
          <ol style={{ display: "flex", flexWrap: "wrap" }}>
            {/* <List>Recommended capital - from 50000</List> */}
            <List>Min. lot {account?.min_lot}</List>
            <List>Max. lot {account?.max_lot}</List>
            <List>{account?.max_open_trade} Open Orders</List>
            <List>{account?.order_history} order history</List>
            <List>{account?.execution} execution</List>
            <List>Fixed spread from {account?.spread} Points</List>
            <List>
              From {account?.min_deposit}
              {currency} per lot
            </List>
            <List>Limit & stop levels {account?.limit_stop}</List>
          </ol>
        </CardBody>
      </Card>

      <Modal isOpen={showModal} backdrop={true} centered={true}>
        <ModalHeader toggle={toggle}>MetaTrader 5</ModalHeader>
        <ModalBody>
          <div>
            <h6>Demo Account created</h6>
            <ul>
              <li>Terminal Login: {result?.account_login}</li>
              <li>Main Password: {result?.password_main}</li>
              <li>Investor Password: {result?.password_investor}</li>
              <li>Phone Password: {result?.password_phone}</li>
              <li>Server Name: {result?.plan?.group}</li>
              <li>Account Type: {result?.account_name}</li>
              <li>Account Currency: {result?.account_name}</li>
              <li>Account Leverage: 1:{result?.leverage}</li>
              <li>Balance Amount: {result?.balance}</li>
            </ul>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button
            onClick={() => {
              navigate(`${process.env.PUBLIC_URL}/platform`)
            }}
          >
            Download MT5
          </Button>
        </ModalFooter>
      </Modal>
    </>
  )
}

export default LiveForm
