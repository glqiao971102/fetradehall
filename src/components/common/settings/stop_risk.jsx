import React from "react"
import { useForm } from "react-hook-form"
import { Form, FormGroup, Label, Input, Button } from "reactstrap"
import { useSelector } from "react-redux"

import AccountService from "../../../network/services/account"

const ChangeStopRisk = () => {
  const { register, handleSubmit, errors } = useForm()
  const account = useSelector((state) => state.dashboard.account)

  const handleChange = async (data) => {
    console.log(data)

    try {
      if (data !== "" && account != null) {
        const result = await AccountService.changeStopRisk(account.id, data)
        console.log(result)
      } else {
        console.log("error")
        errors.showMessages()
      }
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div>
      <p>
        Your stop risk % for this account is {account?.stop_risk ?? "-"}%. If you need to change the stop
        risk, Your account should have no open orders.
      </p>

      <Form className="theme-form" onSubmit={handleSubmit(handleChange)}>
        <FormGroup>
          <Label>MT5 Account Number</Label>
          <Input
            className="form-control"
            type="text"
            name="account_login"
            defaultValue={account?.account_login}
            readOnly
          />
        </FormGroup>

        <FormGroup>
          <Label>MT5 Master Password</Label>
          <Input
            className="form-control"
            type="text"
            name="master_password"
            innerRef={register({ required: true })}
          />
          <span style={{ color: "red" }}>
            {errors.master_password && "Master password is required"}
          </span>
        </FormGroup>

        <FormGroup>
          <Label>Stop Risk</Label>
          <Input
            className="form-control"
            type="text"
            name="stop_risk"
            innerRef={register({ required: true })}
          />
          <span style={{ color: "red" }}>{errors.levarage && "Stop risk is required"}</span>
        </FormGroup>

        <FormGroup className="mb-0" style={{ float: "right" }}>
          <Button color="primary">Confirm</Button>
        </FormGroup>
      </Form>
    </div>
  )
}

export default ChangeStopRisk
