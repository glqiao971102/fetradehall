import React, { useState, Fragment } from "react"
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap"
import { useDispatch } from "react-redux"

const DemoSettings = ({ account, setSelectedSetting, settings }) => {
  const dispatch = useDispatch()

  // dropdown
  const [dropdownOpen, setDropdownOpen] = useState(false)
  const toggle = () => setDropdownOpen((prevState) => !prevState)

  return (
    <div className="dropdown-account">
      <Dropdown isOpen={dropdownOpen} toggle={toggle}>
        <DropdownToggle caret color="secondary">
          Settings
        </DropdownToggle>
        <DropdownMenu className="dropdown-content">
          {settings.map((setting, index) => {
            return (
              <Fragment key={setting.id}>
                <DropdownItem
                  onClick={() => {
                    dispatch({
                      type: "SET_DASHBOARD_ACCOUNT",
                      account: account,
                    })
                    setSelectedSetting(index)
                  }}
                >
                  {setting.title}
                </DropdownItem>
                {index !== settings.length - 1 && <DropdownItem divider />}
              </Fragment>
            )
          })}
        </DropdownMenu>
      </Dropdown>
    </div>
  )
}

export default DemoSettings
