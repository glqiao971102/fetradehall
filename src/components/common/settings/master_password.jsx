import React from "react"
import { useForm } from "react-hook-form"
import { Form, FormGroup, Label, Input, Button } from "reactstrap"
import { useSelector } from "react-redux"

import AccountService from "../../../network/services/account"

const MasterPassword = () => {
  const { register, handleSubmit, errors } = useForm()
  const account = useSelector((state) => state.dashboard.account)

  const handleChangePassword = async (data) => {
    console.log(data)

    try {
      if (data !== "" && account != null) {
        const result = await AccountService.changeMasterPassword(account.id, data)
        console.log(result)
      } else {
        console.log("error")
        errors.showMessages()
      }
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div>
      <Form className="theme-form" onSubmit={handleSubmit(handleChangePassword)}>
        <FormGroup>
          <Label>MT5 Account Number</Label>
          <Input
            className="form-control"
            type="text"
            name="account_login"
            defaultValue={account?.account_login}
            readOnly
          />
        </FormGroup>

        <FormGroup>
          <Label>New Password</Label>
          <Input
            className="form-control"
            type="text"
            name="new_password"
            innerRef={register({ required: true })}
          />
          <span style={{ color: "red" }}>{errors.new_password && "New password is required"}</span>
        </FormGroup>

        <FormGroup className="mb-0" style={{ float: "right" }}>
          <Button color="primary">Confirm</Button>
        </FormGroup>
      </Form>
    </div>
  )
}

export default MasterPassword
