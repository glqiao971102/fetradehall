import React from "react"
import { useForm } from "react-hook-form"
import { Form, FormGroup, Label, Input, Button } from "reactstrap"
import { useSelector } from "react-redux"

import AccountService from "../../../network/services/account"

const ChangeLeverage = () => {
  const { register, handleSubmit, errors } = useForm()
  const account = useSelector((state) => state.dashboard.account)

  const handleChange = async (data) => {
    console.log(data)

    try {
      if (data !== "" && account != null) {
        const result = await AccountService.changeLeverage(account.id, data)
        console.log(result)
      } else {
        console.log("error")
        errors.showMessages()
      }
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div>
      <p>
        Your leverage for this account is 1:{account?.leverage ?? "-"}. If you need to change the
        leverage, Your account should have no open orders.
      </p>

      <Form className="theme-form" onSubmit={handleSubmit(handleChange)}>
        <FormGroup>
          <Label>MT5 Account Number</Label>
          <Input
            className="form-control"
            type="text"
            name="login"
            defaultValue={account?.account_login}
            readOnly
          />
        </FormGroup>

        {/* <FormGroup>
          <Label>MT5 Master Password</Label>
          <Input
            className="form-control"
            type="text"
            name="master_password"
            innerRef={register({ required: true })}
          />
          <span style={{ color: "red" }}>
            {errors.master_password && "Master password is required"}
          </span>
        </FormGroup> */}

        {/* <FormGroup>
          <Label>Leverage</Label>
          <Input
            className="form-control"
            type="text"
            name="leverage"
            innerRef={register({ required: true })}
          />
          <span style={{ color: "red" }}>{errors.leverage && "Leverage password is required"}</span>
        </FormGroup> */}

        <FormGroup>
          <Label>Leverage</Label>
          <Input
            type="select"
            name="leverage"
            className="form-control digits"
            innerRef={register({ required: true })}
          >
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="200">200</option>
          </Input>
        </FormGroup>

        <FormGroup className="mb-0" style={{ float: "right" }}>
          <Button color="primary">Confirm</Button>
        </FormGroup>
      </Form>
    </div>
  )
}

export default ChangeLeverage
