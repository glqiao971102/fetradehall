import client from "../request"
import _ from "lodash"

const getAll = (props) => {
  let page = props?.page ?? 1
  let limit = props?.limit ?? 10
  let filters = props?.filters ?? null
  let filterString = ""

  if (!_.isEmpty(filters)) {
    console.log("?")
    _.forEach(filters, function (value, key) {
      filterString += `&${key}=${value}`
    })
  }

  return client.get(`/me/clients?page=${page}&limit=${limit}${filterString}`)
}

export default {
  getAll,
}
