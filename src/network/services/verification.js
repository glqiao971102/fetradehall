import client from "../request"

const create = (data) => {
  return client.post("/me/verifications", data)
}

export default {
  create,
}
