import client from "../request"

const getAll = () => {
  return client.get(`/platforms`)
}

const get = (id) => {
  return client.get(`/platforms/${id}`)
}

export default {
  getAll,
  get,
}
