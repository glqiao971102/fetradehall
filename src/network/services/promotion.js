import client from "../request"

const getAll = (data) => {
  return client.get("/promotions")
}

export default {
  getAll,
}
