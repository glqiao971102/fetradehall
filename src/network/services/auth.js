import client from "../request";

const login = (data) => {
  return client.post("/auth/login", data);
};

const register = (data) => {
  return client.post(`/auth/register`, data);
};

const logout = () => {
  localStorage.clear();
  window.location.reload();
};

const sendVerification = (data) => {
  return client.post(`/auth/code`, data);
};

const verifyCode = (data) => {
  return client.post(`/auth/code/verify`, data);
};

export default {
  login,
  register,
  logout,
  sendVerification,
  verifyCode,
};
