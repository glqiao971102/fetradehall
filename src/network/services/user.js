import client from "../request"
import _ from "lodash"

const getMyself = () => {
  return client.get("/me")
}

const getCommissions = (props = null) => {
  let page = props?.page ?? 1
  let limit = props?.limit ?? 10

  return client.get(`/me/commissions?page=${page}&limit=${limit}`)
}

const getBonuses =  (props = null) => {
  let page = props?.page ?? 1
  let limit = props?.limit ?? 10
  let filters = props?.filters ?? null
  let filterString = ""

  if (!_.isEmpty(filters)) {
    _.forEach(filters, function (value, key) {
      filterString += `&${key}=${value}`
    })
  }

  return client.get(`/me/bonuses?page=${page}&limit=${limit}${filterString}`)
}

const createProfile = (data) => {
  return client.post("/me/profiles", data)
}

const updateProfile = (id, data) => {
  return client.post(`/me/profiles/${id}`, data)
}

const changePassword = (data) => {
  return client.post(`/auth/change-password`, data)
}

const update2fa = (data) => {
  return client.post(`/me/two_factor`, data)
}

const createBank = (data) => {
  return client.post("/me/banks", data)
}

const updateBank = (id, data) => {
  return client.put(`/me/banks/${id}`, data)
}

const verifyEmail = (data) => {
  return client.post("/users/available", data)
}

export default {
  getMyself,
  createProfile,
  updateProfile,
  createBank,
  updateBank,
  verifyEmail,
  changePassword,
  update2fa,
  getCommissions,
  getBonuses,
}
