import client from "../request"

const getAll = () => {
  return client.get(`/payments`)
}

const get = (id) => {
  return client.get(`/payments/${id}`)
}

export default {
  getAll,
  get,
}
