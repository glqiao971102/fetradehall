import client from "../request"
import _ from "lodash"

const getAll = (props) => {
  let filters = props?.filters ?? null
  let filterString = ""

  if (!_.isEmpty(filters)) {
    _.forEach(filters, function (value, key) {
      filterString += `&${key}=${value}`
    })
  }

  return client.get(`/me/accounts?foo=bar${filterString}`)
}

const get = (id) => {
  return client.get(`/me/accounts/${id}`)
}

const getTransactions = (props = null) => {
  let page = props?.page ?? 1
  let limit = props?.limit ?? 10
  let filters = props?.filters ?? null
  let filterString = ""

  if (!_.isEmpty(filters)) {
    _.forEach(filters, function (value, key) {
      filterString += `&${key}=${value}`
    })
  }

  return client.get(`/me/account_transactions?page=${page}&limit=${limit}${filterString}`)
}

const getOpenOrders = (id, pagination) => {
  let offset = pagination?.offset ?? 0
  let limit = pagination?.limit ?? 20

  return client.get(`/me/accounts/${id}/mt5/open_orders?offset=${offset}&limit=${limit}`)
}

const getOpenOrdersCount = (id) => {
  return client.get(`/me/accounts/${id}/mt5/open_orders/count`)
}

const getAccountProfit = (id) => {
  return client.get(`/me/accounts/${id}/mt5/profit`)
}

const getClosedOrders = (id, pagination) => {
  let offset = pagination?.offset ?? 0
  let limit = pagination?.limit ?? 20

  return client.get(`/me/accounts/${id}/mt5/closed_orders?offset=${offset}&limit=${limit}`)
}

const deposit = (id, data) => {
  return client.post(`/me/accounts/${id}/deposits`, data)
}

const withdraw = (id, data) => {
  return client.post(`/me/accounts/${id}/withdraws`, data)
}

const changeMasterPassword = (id, data) => {
  return client.put(`/me/accounts/${id}/master_password`, data)
}

const changeInvestorPassword = (id, data) => {
  return client.put(`/me/accounts/${id}/investor_password`, data)
}

const changeLeverage = (id, data) => {
  return client.put(`/me/accounts/${id}/leverage`, data)
}

const changeStopRisk = (id, data) => {
  return client.put(`/me/accounts/${id}/stop_risk`, data)
}

const createDemo = (data) => {
  return client.post(`/me/accounts/demo`, data)
}

const createLive = (data) => {
  return client.post(`/me/accounts/live`, data)
}

export default {
  getAll,
  get,
  deposit,
  withdraw,
  getOpenOrders,
  getClosedOrders,
  getOpenOrdersCount,
  getAccountProfit,
  createDemo,
  createLive,
  getTransactions,
  changeInvestorPassword,
  changeLeverage,
  changeMasterPassword,
  changeStopRisk,
}
