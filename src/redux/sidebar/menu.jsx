import {
  Home,
  Monitor,
  User,
  DollarSign,
  Download,
  Phone,
  Navigation,
  Navigation2,
  BarChart
} from "react-feather"
import Auth from "../../network/services/auth"

export const MENUITEMS = [
  {
    title: "Dashboard",
    icon: Home,
    type: "link",
    path: `${process.env.PUBLIC_URL}/dashboard`,
  },
  {
    title: "Financial Operations",
    icon: DollarSign,
    type: "link",
    path: `${process.env.PUBLIC_URL}/financial`,
  },
  {
    title: "Partner Room",
    icon: User,
    type: "link",
    path: `${process.env.PUBLIC_URL}/partner`,
  },
  // {
  //   title: "Risk Management",
  //   icon: AlertTriangle,
  //   type: "link",
  //   path: `${process.env.PUBLIC_URL}/risk`,
  // },
    {
      title: "Pamm",
      icon: BarChart,
      type: 'link',
      path: `${process.env.PUBLIC_URL}/pamm`
    },

  {
    title: "Monitoring",
    icon: Monitor,
    type: "link",
    path: `${process.env.PUBLIC_URL}/monitoring`,
  },
  {
    title: "Trading Platforms",
    icon: Download,
    type: "link",
    path: `${process.env.PUBLIC_URL}/platform`,
  },
  {
    title: "Customer Support",
    icon: Phone,
    type: "link",
    path: `${process.env.PUBLIC_URL}/support`,
  },
  {
    menuTitle: "MT5",
    menuContent: "Accounts",
    type: "container",
  },
  {
    title: "Demo",
    icon: Navigation,
    type: "link",
    path: `${process.env.PUBLIC_URL}/demo`,
  },
  {
    title: "Live",
    icon: Navigation2,
    type: "link",
    path: `${process.env.PUBLIC_URL}/live`,
  },
  {
    menuTitle: "User",
    menuContent: "setting",
    type: "container",
    mobile: true
  },
  {
    title: "Logout",
    icon: Navigation,
    type: "action",
    action: () => Auth.logout(),
    mobile: true
  },
]
