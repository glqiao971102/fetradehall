import { SET_DEMO_ACCOUNT } from "../actionTypes"

const initial_state = {
  account: 0,
}

export default (state = initial_state, action) => {
  switch (action.type) {
    case SET_DEMO_ACCOUNT:
      return { ...state, account: action.account }

    default:
      return { ...state }
  }
}
